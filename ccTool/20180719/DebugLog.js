/**
 * Created by huangpinchian on 2017/11/10.
 * debug 用 log 工具
 */



var cctool = cctool || {};


(function(module){
    module.DebugLog = true;

    module.owenlog = function () {
        var logstr = "[owen]";
        var argument;
        for (var i = 0; i < arguments.length; i++) {
            if (arguments[i] === null) {
                argument = "<null>";
            } else if (arguments[i] === undefined) {
                argument = "<undefined>";
            } else {
                argument = arguments[i].toString();
            }

            logstr += " " + argument;
        }

        cc.log(logstr);
    };


    /**
     * 取得 16 進位的字串
     * @param val
     * @returns {*}
     */
    module.getStr16 = function (val) {
        if(val === null || val === undefined)
            return "null";
        return "0x" + val.toString(16);
    };


    /**
     *
     * @param utcDate
     */
    module.getUtcDateLog = function (utcDate) { return cctool.getDateLog(new Date(utcDate)); };

    module.getDateLog = function (date) {
        if(!date){
            return "NullDate";
        }
        return date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate() + "-" + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    };


    // 印出堆疊資料
    module.logStack = function (msg, shortPath = true) { cc.log(msg, "Stack:\n" + cctool.getStackLog(shortPath)); };


    // 取得堆疊資料字串
    // shortPath: 是否截短路徑
    module.getStackLog = function (shortPath = true) {
        try {
            throw Error("getStackLog throw Error");
        } catch (e) {
            var stackStr = e.stack;
            stackStr = subStackLog(stackStr, "DebugLog.js"); //把前面 DebugLog.js 部分堆疊拿掉

            if(shortPath)
                stackStr = cctool.toShortPath(stackStr);

            return stackStr;
        }
    };


    // 拿掉堆疊 log 中前面指定的 js 檔堆疊 log
    var subStackLog = function (stackLog, jsFileName) {
        if(!stackLog || !stackLog.indexOf || !jsFileName)
            return stackLog;

        var subOwenStack = function () {
            var owenJsIdx = stackLog.indexOf(jsFileName);
            if(owenJsIdx === -1)
                return;

            var lineIdxA = stackLog.indexOf("\n");
            if(owenJsIdx > lineIdxA)
                return;

            stackLog = stackLog.substring(lineIdxA + 1, stackLog.length);
            subOwenStack();
        };
        subOwenStack();
        return stackLog;
    };


    /**
     * 把路徑截短（多行處理）
     * 允許 pathStr 是多行堆疊資料！
     **/
    var shortStyle = "short"; //"min": 最短, "short": 保留專案名稱之後路徑, "folder": 保留目標檔案前幾層目錄（看 folderStyleVal 變數值）
    var folderStyleVal = 1;
    var toShortPath_strKey = "@/Users/";
    var toShortPath_endKey = ".app/";
    module.toShortPath = function (pathStr) {

        if (cc.sys.os === cc.sys.OS_ANDROID) {
            // android 本來就很短，不用處理

        }else if (cc.sys.os === cc.sys.OS_IOS) {

            if(shortStyle === "min"){
                //範例：init@/.../main.js:99:5

                cc.log("[owen] 原始：" + pathStr);

                var checkIdx_str = pathStr.indexOf(toShortPath_strKey);
                while (checkIdx_str !== -1) {
                    checkIdx_str += "/".length;

                    //找結束截短的位置
                    var lineEndIdx = pathStr.indexOf("\n", checkIdx_str);
                    var endKeyStrIdxA = checkIdx_str;
                    var endKeyStrIdxB = checkIdx_str;
                    while (endKeyStrIdxB !== -1 && endKeyStrIdxB <= lineEndIdx) {
                        endKeyStrIdxA = endKeyStrIdxB;
                        endKeyStrIdxB = pathStr.indexOf("/", endKeyStrIdxA + 1);
                    }

                    pathStr = pathStr.substring(0, checkIdx_str) + "..." + pathStr.substring(endKeyStrIdxA, pathStr.length);

                    cc.log("[owen] 處理：" + pathStr);

                    checkIdx_str = pathStr.indexOf(toShortPath_strKey, checkIdx_str);
                }
                cc.log("[owen] 完成：" + pathStr);

            }else if(shortStyle === "short") {
                //範例：init@/Users/.../honghu_v3 iOS.app/src/_owen/20180719/ToolsManager.js:48:31

                var checkIdx_str = pathStr.indexOf(toShortPath_strKey);
                while (checkIdx_str !== -1) {
                    checkIdx_str += toShortPath_strKey.length;
                    var checkIdx_end = pathStr.indexOf(toShortPath_endKey, checkIdx_str);
                    if (checkIdx_end !== -1) {

                        //找結束截短的位置
                        var endKeyStrIdxA = checkIdx_str;
                        var endKeyStrIdxB = checkIdx_str;
                        while (endKeyStrIdxB < checkIdx_end) {
                            endKeyStrIdxA = endKeyStrIdxB;
                            endKeyStrIdxB = pathStr.indexOf("/", endKeyStrIdxA + 1);
                        }


                        pathStr = pathStr.substring(0, checkIdx_str) + "..." + pathStr.substring(endKeyStrIdxA, pathStr.length);

                    }
                    checkIdx_str = pathStr.indexOf(toShortPath_strKey, checkIdx_str);
                }
            }else if(shortStyle === "folder"){
                //範例：init@/.../20180719/main.js:99:5
                //     init@/.../_owen/20180719/main.js:99:5
                //     ...

                //待完成...
                // var checkIdx_str = pathStr.indexOf(toShortPath_strKey);
                // while (checkIdx_str != -1) {
                //     checkIdx_str += "/".length;
                //
                //     var splits = pathStr.split('/');
                //
                //     // pathStr = pathStr.substring(0, checkIdx_str) + "..." + pathStr.substring(endKeyStrIdxA, pathStr.length);
                //     pathStr = pathStr.substring(0, checkIdx_str) + "...";
                //     for(var i=splits.length-1; i>=splits.length-1-folderStyleVal; i--){
                //         if(i<0)
                //             break;
                //     }
                //
                //     checkIdx_str = pathStr.indexOf(toShortPath_strKey, checkIdx_str);
                // }

            }
        }

        return pathStr;
    };


    // 印出 object 資料
    module.logObj = function (msg, obj, withFunc = false) {
        var logs = msg + " " + cctool.getObjLog(obj, withFunc);
        cc.log(logs);
    };


    // 忽略的變數名稱
    var IgnorePropertyNames = [
        "parent",
        "startRadius",      //出現 Particle Mode should be Radius 錯誤，且斷言失敗
        "startRadiusVar",   //出現 Particle Mode should be Radius 錯誤，且斷言失敗
        "endRadius",        //出現 Particle Mode should be Radius 錯誤，且斷言失敗
        "endRadiusVar",     //出現 Particle Mode should be Radius 錯誤，且斷言失敗
        "rotatePerS",       //出現 Particle Mode should be Radius 錯誤，且斷言失敗
        "rotatePerSVar",       //出現 Particle Mode should be Radius 錯誤，且斷言失敗
        "__coverLayerA",
        "__coverLayerB",
        "__coverLayerC",
        "__coverLayerD",
        "__coverLayerE",
        "getScale",  //可能出現 Exception (當 x != y)
        "scale",   //可能出現 Exception (當 x != y)
        // "getTexture",   //可能出現 Exception
    ];

    // 取得指定物件的所有屬性方法資料（包含父類別）
    // 如果 obj 是底層類別，有可能會跳 Invalid Native Object 的錯誤訊息！
    module.getObjLog = function (obj, withFunc = false) {

        var unitTabStr = "    ";    //單層縮排
        var logs = "[logObj] ";

        var logedObjs = [];

        function getObjString(tar) {
            if (tar === undefined)
                return "undefined";

            if (tar === null)
                return "null";

            var tarClassName = cctool.tryGetClassName(tar);

            if(tarClassName){
                return "{" + tarClassName + "}" + tar.toString();
            }else{
                return tar.toString();
            }
        }

        // 加入指定物件的 log（待處理當父子類別出現同變數名稱時的狀況！)
        // tar_object 指定物件的最終子類別物件
        // tar_parent 指定物件的每一層父類別物件，第一次 tar_object == tar_parent（實際值要用子類別取，所以要分開）
        function addLog(tar_object, tar_parent, tab) {
            var objIdx = logedObjs.indexOf(tar_parent);
            var haveShowed = (objIdx !== -1);
            if (haveShowed) {
                logs += "[! 已印過，只顯示變數值 ~" + objIdx + "~ !]";    //__proto__ 物件會一直重複，還是要印值
                // return;
            }

            var tabStr = "";
            for (var i = 0; i < tab; i++)
                tabStr += unitTabStr;

            logs += getObjString(tar_parent).replace(/\n/g, "\n" + tabStr + unitTabStr + unitTabStr) + " ~" + logedObjs.length + "~";  //換行多2層

            logedObjs.push(tar_parent);

            var loopField = function (fieldName) {
                // cc.log("取得 field: " + fieldName);
                if(IgnorePropertyNames.indexOf(fieldName) !== -1){
                    logs += "\n" + tabStr + "[" + fieldName + "] = [! 已被設定忽略 !]";
                    return true;
                }

                var isSameFieldName = false;    //待處理當父子類別出現同變數名稱時的狀況用

                // var isErr = false;
                var errMes = null;
                var isObj = false;
                var isFunc = false;


                try{
                    // isObj = cc.isObject(tar_object_fieldVal);
                    // isFunc = cc.isFunction(tar_object_fieldVal);
                    isObj = cc.isObject(tar_object[fieldName]);
                    isFunc = cc.isFunction(tar_object[fieldName]);
                }catch (e){
                    errMes = e.message;
                    // isErr = true;
                    // cc.log("有進 catch", fieldName);
                }

                if(!withFunc && isFunc)  //指定排除方法
                    return true;
                if(haveShowed) {    //重複的物件，排除一些 log
                    if(isFunc)
                        return true;

                    if(fieldName === "__proto__") {
                        //父類別物件要顯示

                    }else {
                        try{
                            var childObjIdx = logedObjs.indexOf(tar_object[fieldName]);
                            if(childObjIdx !== -1){
                                logs += "\n" + tabStr + "[" + fieldName + "] = [! 已完整印過 ~" + childObjIdx + "~ !]";
                                return true;
                            }
                        }catch (e){
                            logs += "\n" + tabStr + "[" + fieldName + "] = [! 取得失敗 !]";
                            return true;
                        }
                    }
                }

                logs += "\n" + tabStr + "[" + fieldName + "] = ";

                if(errMes){
                    logs += "[! " + errMes + " !]";

                }else if (isObj) {
                    if(fieldName === "__proto__") {
                        addLog(tar_object, tar_parent[fieldName], tab + 1);    //只有 __proto__ 物件用父層取，不然會無限迴圈
                    }else if(isSameFieldName){
                        //待補重複的變數名稱狀況
                    }else{
                        var fieldVal = null;
                        try{
                            fieldVal = tar_object[fieldName];    //其他的用子層取，不然可能會出現 Invalid Native Object 錯誤
                        }catch (e){
                            logs += "\n" + tabStr + "[" + fieldName + "] = [! 取得失敗 !]";
                            return true;
                        }
                        addLog(fieldVal, fieldVal, tab + 1);
                    }

                } else if (isFunc) {
                    var funcStr = null;
                    if(isSameFieldName){
                        //待補重複的變數名稱狀況
                    }else{
                        funcStr = tar_object[fieldName].toString();    //方法用子層取，不然可能會出現 Invalid Native Object 錯誤
                    }
                    var funcIsNativeCode = funcStr.indexOf("[native code]");
                    var paramCount = tar_object[fieldName].length;
                    var paramStr = funcStr.substring(funcStr.indexOf("(")+1, funcStr.indexOf(")"));
                    if(paramStr === ""){
                        if(paramCount > 0){
                            logs += "function (" + paramCount + ")";
                        }else{
                            logs += "function ()";
                        }
                    }else{
                        logs += "function (" + paramStr + ")";
                    }
                    if(funcIsNativeCode)
                        logs += " { native code }";

                    // logs += "function " + funcStr.substring(funcStr.indexOf("("), funcStr.indexOf(")")+1);
                    // //判斷是否為原生 code
                    // if(funcStr.indexOf("[native code]"))
                    //     logs += " { native code }";

                } else {
                    var fieldVal = null;
                    if(isSameFieldName){
                        //待補重複的變數名稱狀況
                    }else{
                        fieldVal = tar_object[fieldName];  //資料用子層取，不然可能會出現 Invalid Native Object 錯誤
                    }
                    if (fieldVal === "") {
                        logs += "\"\"";
                    } else if (Number.isInteger(fieldVal)) {
                        logs += fieldVal + " «" + cctool.getStr16(fieldVal) + "»";
                    } else {
                        logs += fieldVal;
                    }
                }
                return true;
            };
            cctool.loopAllPropertyNames(tar_parent, loopField, true, true);
        };

        addLog(obj, obj, 0);
        return logs;
    };


    /**
     * 顯示場景上目標物件資訊
     * @param target
     * @constructor
     */
    module.LogView = function (mes, target) {
        cc.log(mes, cctool.GetViewLog(target));
    };

    /**
     * 取得場景上目標物件資訊
     * @param target
     * @constructor
     */
    module.GetViewLog = function (target) {

        /** 第一行標頭 **/
        var mesLog = "物件訊息：[" + cctool.GetInstNID(target) + "] ";
        var targetClassName = cctool.tryGetClassName(target);
        if(targetClassName){
            mesLog += "{" + targetClassName + "}";
        }else{
            mesLog += target;
        }
        //文字內容第一行
        if(target.getString){
            var str = target.getString();
            if(str) {
                var strSplit = str.split('\n');
                if(strSplit.length === 1){
                    mesLog += " \"" + strSplit[0] + "\"";
                }else{
                    mesLog += " \"" + strSplit[0] + " ...\"";
                }
            }
        }

        var spriteFrameName = null;
        var spriteTextureName = null;
        if(cctool.ViewObjCatchData) {
            spriteFrameName = cctool.GetSpriteFrameName(target);
            spriteTextureName = cctool.GetTextureName(target);
        }
        if(spriteFrameName)
            mesLog += " \"" + spriteFrameName + "\"";
        if(spriteTextureName)
            mesLog += " \"" + spriteTextureName + "\"";

        /** 提示 **/
        if(!cctool.ViewObjCatchData)
            mesLog += "\n\t< 開啟 ViewObjCatchData 套件可能取得 sprite 圖片訊息 >";

        /** 基本資訊 **/
        //基本資料
        if(target.__coverLayerA) {
            var color = target.__coverLayerA.color;
            mesLog += "\n\t[準心顏色]：" + color.r + "," + color.g + "," + color.b + ", alpha: " + target.__coverLayerA.opacity;
        }
        color = target.color;
        if(color) {
            mesLog += "\n\t[物件顏色]：" + color.r + "," + color.g + "," + color.b + ", alpha: " + target.opacity;
        }else{
            mesLog += "\n\t[物件顏色]：無法設定, alpha: " + target.opacity;
        }
        mesLog += "\n\t[Position]：" + target.x + ", " + target.y;
        mesLog += "\n\t[Scale]：" + target.scaleX + ", " + target.scaleY;
        mesLog += "\n\t[Anchor]：" + target.anchorX + ", " + target.anchorY;

        if(target.getSize)
            mesLog += "\n\t[Size]：" + target.getSize().width + ", " + target.getSize().height;
        if("fontSize" in target)
            mesLog += "\n\t[fontSize]：" + target.fontSize;
        if(target.getFontSize)
            mesLog += "\n\t[FontSize]：" + target.getFontSize();
        if(target.getBMFontSize)
            mesLog += "\n\t[BMFontSize]：" + target.getBMFontSize();
        if(target.getSystemFontSize)
            mesLog += "\n\t[SystemFontSize]：" + target.getSystemFontSize();
        if(target.getVirtualRendererSize)
            mesLog += "\n\t[VirtualRendererSize]：" + target.getVirtualRendererSize() + ", " + target.getVirtualRendererSize().width + ", " + target.getVirtualRendererSize().height;


        if(target.getContentSize)
            mesLog += "\n\t[CostonSize]：" + target.getContentSize().width + ", " + target.getContentSize().height;

        if(target.getString)
            mesLog += "\n\t[GetString]：" + target.getString();


        // if(cctool.ViewObjCatchData){
        //     if(spriteFrameName)
        //         mesLog += "\n\t[SpriteFrameName]：" + cctool.GetSpriteFrameName(target);
        //     if(spriteTextureName)
        //         mesLog += "\n\t[SpriteTextureName]：" + cctool.GetTextureName(target);
        //
        // }else{
        //     mesLog += "\n\t< 開啟 ViewObjCatchData 套件可能取得 sprite 圖片訊息 >";
        // }


        //父子階層資料（第一次有點慢）
        if(false){
            mesLog += "\n\t[父物件表]：會頓，目前設定關閉";
        }else{
            // var parentLog = "<Target>";
            // var parent = target.parent;
            // // var parent = target;
            // while(parent){
            //     var parentClassName = cctool.tryGetClassName(parent);
            //     if(parentClassName !== null) {
            //         parentLog = "[" + cctool.GetInstNID(parent) + "] {" + parentClassName + "}" + " / " + parentLog;
            //     }else if(parent.name){
            //         parentLog = "[" + cctool.GetInstNID(parent) + "] " + parent.name + " / " + parentLog;
            //     }else{
            //         parentLog = "[" + cctool.GetInstNID(parent) + "] " + parent.toString() + " / " + parentLog;
            //     }
            //
            //     parent = parent.parent;
            // }
            mesLog += "\n\t[父物件表]：" + cctool.getParentsLog(target);
        }

        /** 外部帶入的額外資訊 **/
        if (target.__showTargetCenter_mes) {
            for (var i = 0; i < target.__showTargetCenter_mes.length; i++)
                mesLog += "\n\t[點擊訊息" + i + "]：" + target.__showTargetCenter_mes[i];
        }

        /** 加入場景配置資料（慢） **/
        if(true){
            mesLog += "\n\t[當前配置]：" + cctool.GetHierarchyLog_Scene([target]);
        }else{
            mesLog += "\n\t[當前配置]：很長，需要再開。";
        }


        return mesLog;
    };

    module.getParentsLog = function (target) {
        if(target === null)
            return "<Null no parent>";
        if(target === undefined)
            return "<Undefined no parent>";
        var parentLog = "<Target>";
        var parent = target.parent;
        while(parent) {
            var parentClassName = cctool.tryGetClassName(parent);
            if (parentClassName !== null) {
                parentLog = "[" + cctool.GetInstNID(parent) + "] {" + parentClassName + "}" + " / " + parentLog;
            } else if (parent.name) {
                parentLog = "[" + cctool.GetInstNID(parent) + "] " + parent.name + " / " + parentLog;
            } else {
                parentLog = "[" + cctool.GetInstNID(parent) + "] " + parent.toString() + " / " + parentLog;
            }

            parent = parent.parent;
        }
        return parentLog;
    };


    // 額外完全無法尋訪的變數名稱
    // var CheckPropertyNames = [];
    var CheckPropertyNames = [
        "__proto__",
        "prototype",
        "constructor",
        "responseText",  // XMLHttpRequest 類得變數
        "responseXML"  // XMLHttpRequest 類得變數
    ];

    // 尋訪單一物件所有變數名稱
    //  參考：https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Enumerability_and_ownership_of_properties
    // obj : 目標物件
    // loopFunc : 尋訪方法，並傳回是否繼續尋訪
    // iterateSelfBool : 尋訪設定為可尋訪的變數
    // iteratePrototypeBool : 尋訪被設為不可尋訪的變數
    module.loopAllPropertyNames = function (obj, loopFunc, iterateSelfBool = true, iteratePrototypeBool = true) {
        if (obj === null || obj === undefined || !loopFunc) {
            return;
        }
        if (!iterateSelfBool && !iteratePrototypeBool) {
            return;
        }

        var props = [];
        var checkProp = function (prop) {
            if (props.indexOf(prop) !== -1)
                return true;

            props.push(prop);
            var res = loopFunc(prop);
            return res;
            // return loopFunc(prop);
        };

        //包含 Enumerable 跟 Nonenumerable 的屬性
        var allNames = null;
        try {
            allNames = Object.getOwnPropertyNames(obj);
        }catch (e){
            cc.log("Error 物件無法使用 Object.getOwnPropertyNames 取得變數列表！\nmessage：", e.message, "\nobj：", obj, "\nstack：", e.stack);
        }
        if(allNames) {
            for (var i = 0; i < allNames.length; i++) {
                var prop = allNames[i];
                if(IgnorePropertyNames.indexOf(prop) !== -1){
                    //不判斷，後面會忽略
                }else if (obj.propertyIsEnumerable(prop)) {
                    if (!iterateSelfBool) {
                        continue;
                    }
                } else {
                    if (!iteratePrototypeBool) {
                        continue;
                    }
                }

                if (!checkProp(prop)) {
                    return;
                }
            }
        }else{
            for(var prop in obj){
                if (!checkProp(prop)) {
                    return;
                }
            }
        }

        //其他完全無法尋訪的特別指定變數
        if(obj && !cc.isString(obj) && iteratePrototypeBool){
            for(var i=0; i<CheckPropertyNames.length; i++){
                var prop = CheckPropertyNames[i];
                if(IgnorePropertyNames.indexOf(prop) !== -1) {
                    //不判斷，後面會忽略
                }else {
                    try {
                        if (!(prop in obj))
                            continue;
                    } catch (e) {
                        cc.log("無法判斷變數名稱是否存在：", prop, " mes:", e.message);
                    }
                }

                if(!checkProp(prop))
                    return;
            }
        }
    };




    // //印出 byte packet 的指令（預設在第 5 個 byte 開始的 int 值）
    // var getPacketCommand = function (packet) {
    //     var orgRPos = packet._rpos;
    //     packet._rpos = 0;
    //     packet.readskip(5);
    //     var command = packet.readInt();
    //     packet._rpos = orgRPos;
    //     return command;
    // };
    // module.getPacketCommand = getPacketCommand;


    // 印出 byte packet 資料
    module.logByte = function (msg, packet) {
        cc.log(msg + " " + cctool.getByteLog(packet));
    };


    //取得 Byte Log 字串
    // style: 0 = "01 02 03...", 1 = "0x00, 0x01, 0x02..."
    module.getByteLog = function (packet, style, addTag = true) {
        var logs = "";
        if (addTag)
            logs += "[logByte " + packet._rpos + ":" + packet._wpos + "] ";

        var orgRPos = packet._rpos;

        packet._rpos = 0;

        while (packet._rpos < packet._wpos) {
            if (packet._rpos > 0)
                logs += " ";
            var byteStr = packet.readByte().toString(16);
            if (byteStr.length < 2)
                byteStr = "0" + byteStr;
            if (packet._rpos === orgRPos) {
                logs += "[" + byteStr + "]"; //標示當前讀取位置
            } else {
                logs += byteStr;
            }
        }

        packet._rpos = orgRPos;
        if(style === 1)
            logs = "0x" + logs.replace(/ /g, ", 0x").replace(/]/g, "").replace(/\[/g, "");

        return logs
    };

    /**
     *  檢查物件是否為指定 Class
     *  instObj: 要被檢查的目標實例物件
     *  classObj: 指定的 Class 物件
     *  loopBaseType: 是否檢查目標實例物件的繼承類別
     **/
    module.checkClass = function (instObj, classObj, loopBaseType = true) {
        if (!instObj || !instObj.__proto__)
            return false;

        if (!classObj || !classObj.prototype)
            return false;

        if(!loopBaseType){
            return instObj.__proto__ === classObj.prototype;

        }else{
            while(instObj){
                if(instObj === classObj.prototype)
                    return true;
                instObj = instObj.__proto__;
            }
            return false;
        }
    };

    /**
     *  依據 Class Name 檢查物件是否為指定 Class
     *  instObj: 要被檢查的目標實例物件
     *  className: 指定的 Class Name
     *  loopBaseType: 是否檢查目標實例物件的繼承類別
     **/
    module.checkClassByName = function (instObj, className, loopBaseType = true) {
        return cctool.checkClass(instObj, cctool.GlobalInst[className], loopBaseType);
    };

    /**
     * 取得物件變數，不往父類取值
     * @param obj
     * @param propertyName
     * @returns {*}
     */
    module.getObjPropertyWithoutParentClass = function (instObj, propertyName) {
        // cc.log("[rrr] ccc 111", instObj);
        if(!instObj)
            return undefined;
        // cc.log("[rrr] ccc 222", propertyName in instObj);
        if(!(propertyName in instObj))
            return undefined;           //沒有定義過
        // cc.log("[rrr] ccc 333", instObj.__proto__);
        if(!instObj.__proto__)
            return instObj[propertyName];   //沒有父類或非類別實例
        // cc.log("[rrr] ccc 444", propertyName in instObj.__proto__, instObj[propertyName]);
        if(!(propertyName in instObj.__proto__))
            return instObj[propertyName];   //父類沒有此變數名稱
        // cc.log("[rrr] ccc 555", instObj[propertyName] !== instObj.__proto__[propertyName]);
        if((instObj[propertyName] !== instObj.__proto__[propertyName]))
            return instObj[propertyName];   //與父類變數值不同，相同代表可能都取到父類的變數值
        // cc.log("[rrr] ccc 666", instObj[propertyName], instObj.__proto__[propertyName]);
        return undefined;
    };

    /**
     * 嘗試取得類別名稱（類別第一次找會很久！）
     * @param instObj
     * @returns {string}
     */
    module.tryGetClassName = function (instObj) {
        var className = null;

        if (!instObj || !instObj.__proto__)
            return className;

        /* 確定 ok
        if(instObj.__proto__["__cctool_className"]) {
            if(!instObj.__proto__.__proto__ || !instObj.__proto__.__proto__["__cctool_className"] || (instObj.__proto__["__cctool_className"] !== instObj.__proto__.__proto__["__cctool_className"]))    //只抓一層。避免抓到父累的快取值
                return instObj.__proto__["__cctool_className"];
        }
        /*/// 測試做法
        var catchClassName_inst = cctool.getObjPropertyWithoutParentClass(instObj.__proto__, "__cctool_className");
        if(catchClassName_inst)
            return catchClassName_inst;
        //*/

        var checkClassFunc = function (classNameTitle, checkClassObj, checkClassName) {
            // cctool.owenlog("檢查", checkClassName);


            if(!checkClassObj)
                return true;

            // cc.log("檢查 aaa：", checkClassName);

            if(checkClassObj.prototype){
                /* 確定 ok
                if(checkClassObj.prototype["__cctool_className"]){
                    if(!checkClassObj.prototype.__proto__ || !checkClassObj.prototype.__proto__["__cctool_className"] || (checkClassObj.prototype["__cctool_className"] !== checkClassObj.prototype.__proto__["__cctool_className"])) {    //只抓一層。避免抓到父累的快取值
                        return true;    //有設定過代表檢查過了，上面會直接回傳
                    }
                }
                /*/// 測試做法
                var catchClassName_check = cctool.getObjPropertyWithoutParentClass(checkClassObj.prototype, "__cctool_className");
                if(catchClassName_check)
                    return true;    //有設定過代表檢查過了，上面會直接回傳
                //*/

                Object.defineProperty(checkClassObj.prototype, "__cctool_className", { enumerable: false, value: classNameTitle + checkClassName }); //直接設定存下名稱，且設定為不可被尋訪

                // checkClassObj.prototype["__cctool_className"] = checkClassName; //直接設定存下名稱

            }else{
                // cc.log("檢查 ccc：", checkClassName);
                return true;    //非類別物件
            }

            // cc.log("檢查 ddd：", checkClassName);

            if(cctool.checkClass(instObj, checkClassObj, false)) {
                // instObj.__proto__["__cctool_className"] = checkClassName;
                className = classNameTitle + checkClassName;

                return false;
            }else{
                return true;
            }
        };


        var loopTargetObj = function (target, classNameTitle) {
            try{
                cctool.loopAllPropertyNames(
                    target,
                    function (fieldName) { return checkClassFunc(classNameTitle, target[fieldName], fieldName); },
                    true,
                    true
                );
            }catch(e){ }
        };

        // 尋訪可能存在 Class 物件的物件
        try{ loopTargetObj(cctool.GlobalInst, ""); } catch (e) { } //宣告在 Global 的自定義類別
        try{ loopTargetObj(cc, "cc."); } catch (e) { }         //cocos
        try{ loopTargetObj(ccs, "ccs."); } catch (e) { }        //cocos studio
        try{ loopTargetObj(ccui, "ccui."); } catch (e) { }        //cocos ?


        try{ loopTargetObj(nnGame, "nnGame."); } catch (e) { }     //666 專案物件
        try{ loopTargetObj(NNCommon, "NNCommon."); } catch (e) { }   //666 專案物件
        try{ loopTargetObj(zjhGame, "zjhGame."); } catch (e) { }   //666 專案物件
        try{ loopTargetObj(ZJHCommon, "ZJHCommon."); } catch (e) { }   //666 專案物件
        try{ loopTargetObj(DDZGame, "DDZGame."); } catch (e) { }   //666 專案物件
        try{ loopTargetObj(gcCommon, "gcCommon."); } catch (e) { }   //666 專案物件



        return className;
    };

    module.checkIsObj = function (obj) {
        if(!obj)
            return false;
        return cc.isObject(obj);
    };

    module.checkIsClass = function (obj) {
        if(!obj)
            return false;
        if(!obj["extend"])
            return false;
        if(!obj["prototype"])
            return false;
        // if(!obj["length"])
        //     return false;
        return true;
    };

    module.checkIsFunc = function (obj) {
        if(!obj)
            return false;
        if(obj["extend"])
            return false;
        if(!obj["prototype"])
            return false;
        if(!obj["length"])
            return false;
        return true;
    };


    var instNID = 0;
    var instMap_nid_inst = {};
    /**
     * 取得實例物件的流水號
     * 第一次取得會賦予流水號
     * @param inst
     * @returns {*}
     */
    module.GetInstNID = function (inst) {
        if(!inst)
            return null;

        var catchedNID = cctool.getObjPropertyWithoutParentClass(inst, "__cctool_nid");
        if(catchedNID)
            return catchedNID;

        instNID++;
        Object.defineProperty(inst, "__cctool_nid", { enumerable: false, value: instNID }); //直接設定存下 NID，且設定為不可被尋訪
        instMap_nid_inst[instNID] = inst;

        return instNID;
    };

    /**
     * 依流水號取得實例物件，必須有設定過流水號
     * @param instNID
     * @constructor
     */
    module.GetInstByNID = function (instNID) {
        return instMap_nid_inst[instNID];
    };


})(cctool);



