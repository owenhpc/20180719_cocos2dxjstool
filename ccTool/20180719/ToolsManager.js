/**
 * Created by huangpinqian on 2018/7/19.
 * 工具管理物件
 * 此工具是在 cocos2d-x 3.6 版本下開發
 * 使用方式：
 * 在 main.js 下加入
 cc.game.onStart = function () {
    checkEnableTool();      // <------------------ 加入

    ... 原本執行的程式碼
    cc.LoaderScene.preload(g_resources, function () {
        ...
    }, this);
 };

 // android 在這階段會沒有 cc.log !
 if (cc.log)
    cc.log("have cc.log");

 //加入以下
 var cctool = cctool || null;
 var checkEnableTool = function () {
    var cctool_ver = "20180719";   //使用的工具版號
    var cctool_root = "src/ccTool";
    var cctool_mainJs = cctool_root + "/" + cctool_ver + "/ToolsManager.js";
    if (!cctool && jsb.fileUtils.isFileExist(cctool_mainJs)) {
        require(cctool_mainJs);
        cctool.init(cctool_root, cctool_ver);
    }
 };

 cc.game.run();
 */








var cctool = cctool || {};
cctool.GlobalInst = this;

(function(module) {


    /** 因為工具不會進 git ，當任一個工具的方法有變動到需要外部修正時，版號就要變更！避免當 git 回到舊版造成工具無法使用 **/
    var version = "20180719";


    var pluginNames = [
        "TestClass.js",
        "EventNotifier.js", //獨立工具

        "DebugLog.js",
        "DebugLog_LineNum.js",
        "ShowTargetCenter.js",
        "CreateStackLogTool.js", // 所有物件建立監控
        "EventListenerTool.js",
        "BtnTool.js",
        "DebugLog_LoadJsLog.js",
        "DebugLog_Hierarchy.js",
        "ViewObjCatchData.js",
        "DebugLog_CallMethodInfo.js",
        "ShowChangeSceneLog.js",
    ];

    module.init = function (toolRootPath, moduleVer) {
        if(moduleVer !== version){
            cc.log("[owen] 工具版號不符合，無法使用！指定版號：" + moduleVer + "，要求載入版號：" + version);
            cctool = null;
            return;
        }
        if(module.inited) {
            cc.log("[Error][owen] 重複初始化工具");
            return;
        }

        cc.log("[owen] 準備啟動工具，版號：" + version);

        // cc.loader.loadJs("src/ccTool/" + version, pluginNames,
        //     function (err, data) {
        //         if(err){
        //             if(cctool.logObj){
        //                 cctool.logObj("[Error] 啟動工具失敗！err：", err);
        //             }else{
        //                 cc.log("[Error] 啟動工具失敗！err：" + err);
        //             }
        //         }else{
        //             if(cctool.logObj){
        //                 cctool.logObj("啟動工具成功！data：", data);
        //             }else{
        //                 cc.log("啟動工具成功！data：" + data);
        //             }
        //         }
        //         return;
        //     }
        // );

        for (var i = 0; i < pluginNames.length; i++) {
            try{
                require(toolRootPath + "/" + version + "/" + pluginNames[i]);
                cc.log("[owen] 已啟動工具：" + pluginNames[i]);
            }catch (e){
                cc.log("[Error] 啟動工具失敗：" + toolRootPath + "/" + version + "/" + pluginNames[i] + "\nMessage：" + e.message + "\nStack：" + e.stack + "\n");
            }
        }

        module.inited = true;


        try{
            test();
        }
        catch (e) {
            cc.log("Error Test：", e.message, "\nStack:\n", e.stack);
        }


    };


    /** 呼叫方法 **/
    module.callFunctionWithArguments = function (func, caller, args) {
        try{
            return func.apply(caller, args);
        }catch (e){
            var argsLog = "";
            if(args) {
                argsLog = "[ ";
                for (var i = 0; i < args.length; i++) {
                    var argStr = "?";
                    if(args[i] === null){
                        argStr = "null";
                    }else if(args[i] === undefined){
                        argStr = "undefined";
                    }else{
                        argStr = args[i].toString();
                    }
                    if(i === 0){
                        argsLog += argStr;
                    }else{
                        argsLog += ", " + argStr;
                    }
                }
                argsLog += " ]";
            }else{
                argsLog = "None";
            }
            cc.log("[Error] 呼叫失敗：" + argsLog + " Err Mes：", e.message, "\nStack：", e.stack);
            throw e;
        }
    };

    /** 用 new 實例化物件 **/
    module.newInstWithArguments = function (classObj, args) {
        if (!args || args.length === 0) {
            return new classObj();
        } else if (args.length === 1) {
            return new classObj(args[0]);
        } else if (args.length === 2) {
            return new classObj(args[0], args[1]);
        } else if (args.length === 3) {
            return new classObj(args[0], args[1], args[2]);
        } else if (args.length === 4) {
            return new classObj(args[0], args[1], args[2], args[3]);
        } else if (args.length === 5) {
            return new classObj(args[0], args[1], args[2], args[3], args[4]);
        } else if (args.length === 6) {
            return new classObj(args[0], args[1], args[2], args[3], args[4], args[5]);
        } else if (args.length === 7) {
            return new classObj(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
        } else if (args.length === 8) {
            return new classObj(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
        } else if (args.length === 9) {
            return new classObj(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);
        } else if (args.length === 10) {
            return new classObj(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);
        } else {
            cc.log("Error 參數數量超出！" + args.length);
            return null;
        }
    };

    var test = function () {






    };



})(cctool);
