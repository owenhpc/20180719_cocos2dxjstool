/**
 * Created by huangpinqian on 2018/9/25.
 */

var cctool = cctool || {};
(function(module){
    module.ShowChangeSceneLog = true;


    /** 轉場 Log **/
    var org__cc_director_runScene = cc.director.runScene;
    cc.director.runScene = function () {
        var scene = arguments[0];
        if(scene){
            cc.log("轉場景 runScene:", cctool.tryGetClassName(scene), scene, "\nstack:", cctool.getStackLog());
        }else{
            cc.log("轉場景 runScene: null", "\nstack:", cctool.getStackLog());
        }

        return cctool.callFunctionWithArguments(org__cc_director_runScene, this, arguments);
    };
    var org__cc_director_replaceScene = cc.director.replaceScene;
    cc.director.replaceScene = function () {
        var scene = arguments[0];
        if(scene){
            cc.log("轉場景 replaceScene:", cctool.tryGetClassName(scene), scene, "\nstack:", cctool.getStackLog());
        }else{
            cc.log("轉場景 replaceScene: null", "\nstack:", cctool.getStackLog());
        }

        return cctool.callFunctionWithArguments(org__cc_director_replaceScene, this, arguments);
    };

})(cctool);


