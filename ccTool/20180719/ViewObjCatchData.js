/**
 * Created by huangpinqian on 2018/8/16.
 * 物件快取額外資料功能
 */


var cctool = cctool || {};
(function(module){
    module.ViewObjCatchData = true;


    module.GetSpriteFrameName = function (sprite) {
        if(!sprite)
            return null;
        return sprite["__cctool_spriteFrameName"];
    };

    module.GetTextureName = function (sprite) {
        if(!sprite)
            return null;
        return sprite["__cctool_textureName"];
    };


    /** 攔截用 new 建立 Sprite 時的參數 **/
    var org_cc_sprite = cc.Sprite;
    //設定成一個委派的建構子
    cc.Sprite = function () {
        var inst = cctool.newInstWithArguments(org_cc_sprite, arguments);  //用 new 實例化

        var arg0 = arguments[0];    //擷取建立時帶入的 "圖檔路徑" 資料
        if(arg0 && cc.isString(arg0)){
            //存到實例中
            if(arg0[0] === '#'){
                inst["__cctool_spriteFrameName"] = arg0;
            }else{
                inst["__cctool_textureName"] = arg0;
            }
        }

        return inst;
    };
    //把舊有的屬性跟方法連接到委派的建構子
    cctool.loopAllPropertyNames(org_cc_sprite, function (prop) {
        cc.Sprite[prop] = org_cc_sprite[prop];
        return true;
    }, true, true);




    /** 讓 Sprite 存下 spriteFrameName **/
    var org_sprite_create = cc.Sprite.create;
    cc.Sprite.create = function () {
        var inst = cctool.callFunctionWithArguments(org_sprite_create, this, arguments);
        if(inst && arguments && arguments.length > 0) {
            inst["__cctool_spriteFrameName"] = arguments[0];
        }
        return inst;
    };
    var org_sprite_setSpriteFrameName = cc.Sprite.prototype.setSpriteFrame;
    cc.Sprite.prototype.setSpriteFrame = function () {
        delete this["__cctool_textureName"];
        if(arguments && arguments.length > 0) {
            this["__cctool_spriteFrameName"] = arguments[0];
        }else{
            delete this["__cctool_spriteFrameName"];
        }
        return cctool.callFunctionWithArguments(org_sprite_setSpriteFrameName, this, arguments);
    };

    /** 讓 Sprite 存下 textureName **/
    var org_sprite_createWithTexture = cc.Sprite.createWithTexture;
    cc.Sprite.createWithTexture = function () {
        var inst = cctool.callFunctionWithArguments(org_sprite_createWithTexture, this, arguments);
        if(inst && arguments && arguments.length > 0) {
            inst["__cctool_textureName"] = arguments[0];
        }
        return inst;
    };
    var org_sprite_setTexture = cc.Sprite.prototype.setTexture;
    cc.Sprite.prototype.setTexture = function () {
        delete this["__cctool_spriteFrameName"];
        if(arguments && arguments.length > 0) {
            this["__cctool_textureName"] = arguments[0];
        }else{
            delete this["__cctool_textureName"];
        }
        return cctool.callFunctionWithArguments(org_sprite_setTexture, this, arguments);
    };

})(cctool);


