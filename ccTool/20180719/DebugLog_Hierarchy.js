/**
 * Created by huangpinqian on 2018/8/16.
 * 取得或印出結構階層資料
 */

var cctool = cctool || {};
(function(module){
    module.DebugLog_Hierarchy = true;

    var VisibleSinge_Show = "@";    //正常顯示
    var VisibleSinge_Hide = "X";    //設定為隱藏
    var VisibleSinge_Unknow = "?";    //無法判斷
    var VisibleSinge_ShowButParentHide = "O";   //設定為顯示，但父物件隱藏

    module.HierarchyIgnoreNames = [];    //忽略不顯示的物件名稱

    /**
     * 輸出當前 scene 下所有結構
     * @param mes
     * @param tagTargets
     * @constructor
     */
    module.LogHierarchy_Scene = function (mes, tagTargets = null) {
        cc.log(mes, cctool.GetHierarchyLog_Scene(tagTargets));
    };

    /**
     * 取得當前 scene 下所有結構字串
     * @param tagTargets
     * @constructor
     */
    module.GetHierarchyLog_Scene = function (tagTargets = null) {
        return cctool.GetHierarchyLog(cc.director.getRunningScene(), tagTargets);
    };

    /**
     * 取得指定物件下所有結構
     * @param rootObj   根物件目標
     * @param tagTargets   特別標示的物件陣列，會用 "<---------" 標示出來
     * @returns {string}
     */
    module.GetHierarchyLog = function (rootObj, tagTargets = null) {
        if(!rootObj)
            return;
        if(!rootObj.getChildren)
            return;

        var log = "[Hierarchy] " + cctool.tryGetClassName(rootObj) + " :";
        log += "\n < " + VisibleSinge_Show + ": 正常顯示 >";
        log += "\n < " + VisibleSinge_Hide + ": 設定為隱藏 >";
        log += "\n < " + VisibleSinge_ShowButParentHide + ": 設定為顯示，但父物件隱藏 >";
        log += "\n < " + VisibleSinge_Unknow + ": 無法判斷是否隱藏 >";
        if(cctool.ShowTargetCenter)
            log += "\n < +: 有建立準心，有開啟 ShowTargetCenter 套件才有 >";
        if(!cctool.ViewObjCatchData)
            log += "\n < 開啟 ViewObjCatchData 套件可能取得 sprite 圖片訊息 >";


        var loopTarget = function (target, step, parentVisible) {

            /** 判斷忽略 **/
            if(target.name && cctool.HierarchyIgnoreNames.indexOf(target.name) !== -1)
                return; //設定為忽略

            var stepStr = "";
            for(var i=0; i<step; i++)
                stepStr += "\t";

            log += "\n" + stepStr + "[" + cctool.GetInstNID(target) + "] ";

            if(cctool.ShowTargetCenter){
                if(cctool.CheckHaveConcentric(target)){
                    log += "+";
                }else{
                    log += " ";
                }
            }

            /** visible 顯示標誌 **/
            var targetVisible = true;
            if(target.visible === true){
                if(parentVisible){
                    log += VisibleSinge_Show + " ";
                }else{
                    log += VisibleSinge_ShowButParentHide + " ";
                }
            }else if(target.visible === false){
                targetVisible = false;
                log += VisibleSinge_Hide + " ";
            }else{
                targetVisible = false;
                log += VisibleSinge_Unknow + " ";
            }

            /** 類別或名稱 **/
            var targetClassName = cctool.tryGetClassName(target);
            if(targetClassName){
                log += "{" + cctool.tryGetClassName(target) + "}";
            }else{
                log += target;
            }
            if(target.name)
                log += " " + target.name + "";

            /** 圖片名稱 **/
            if(cctool.ViewObjCatchData){
                if(cctool.GetSpriteFrameName(target))
                    log += " \"" + cctool.GetSpriteFrameName(target) + "\"";

                if(cctool.GetTextureName(target))
                    log += " \"" + cctool.GetTextureName(target) + "\"";
            }
            /** 文字內容 **/
            if(target.getString){
                var str = target.getString();
                if(str) {
                    str = str.toString();   //有時候不是 string !
                    var strSplit = str.split('\n');
                    if(strSplit.length === 1){
                        log += " \"" + strSplit[0] + "\"";
                    }else{
                        log += " \"" + strSplit[0] + " ...\"";
                    }
                }
            }

            /** 指定目標的標示 **/
            if(tagTargets && tagTargets.indexOf(target) !== -1)
                log += "  <----------------";


            /** 尋訪子物件 **/
            var children = null;
            if(target.getChildren)
                children = target.getChildren();
            if(!children || children.length === 0)
                return;
            for(var i=0; i<children.length; i++)
                loopTarget(children[i], step+1, targetVisible && parentVisible);
        };

        // var rootObjVisible = (rootObj.getVisible) ? rootObj.getVisible() : true;
        var rootObjVisible = (rootObj.visible) ? rootObj.visible : false;
        loopTarget(rootObj, 0, rootObjVisible);

        return log;
    };


})(cctool);


