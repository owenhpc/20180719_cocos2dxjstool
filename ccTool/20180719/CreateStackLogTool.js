/**
 * Created by huangpinqian on 2018/7/19.
 * 監視指定類型物件的建立堆疊資料工具
 * 會在物件上顯示準心，點擊後顯示資料 Log
 * 可過濾 "物件資訊" 字串來看
 */




var cctool = cctool || {};
(function(module) {
    module.CreateStackLogTool = true;

    module.PauseCreateStackLogTool = false; // 暫停此機制

    // 要追蹤的目標類別
    // cls: 直接指定類別物件
    // clsName: 要追蹤的目標類別（如果不是一開始就存在的類別，就用類別名稱指定）
    // withExtendCls: 是否要連子物件類別一起處理
    // color: 準心顏色
    var targetClasses = [
        // 容器
        { cls: cc.LayerColor,       withExtendCls: false, color: cc.color(0, 255, 0) },
        { cls: cc.Layer,            withExtendCls: false, color: cc.color(0, 255, 0) },
        // { cls: cc.ScrollView,       withExtendCls: false, color: cc.color(0, 0, 255) },

        // 文字
        { cls: cc.LabelTTF,         withExtendCls: true },
        { cls: cc.LabelAtlas,       withExtendCls: true },
        { cls: cc.RichText,         withExtendCls: true },
        { cls: cc.LabelBMFont,      withExtendCls: true },
        { cls: cc.EditBox,          withExtendCls: true },

        // 圖像
        { cls: cc.MenuItemSprite,   withExtendCls: true },
        { cls: cc.Sprite,           withExtendCls: true },
        { cls: cc.Scale9Sprite,     withExtendCls: true },
        { cls: cc.ControlSlider,    withExtendCls: true },      //拖曳 Bar 條
        { cls: cc.ProgressTimer,    withExtendCls: true },

        { cls: ccui.Button,    withExtendCls: true },

        // 粒子
        // { cls: cc.ParticleSystem,   withExtendCls: true },

        // 其他
        // { clsName: "HrShootTouchLayer", withExtendCls: true },
        // { clsName: "HrNormalFish",  withExtendCls: true },
        // { clsName: "HrBasicFish",   withExtendCls: true },
        // { clsName: "BenzAnimalLayer", withExtendCls: true },
        // { clsName: "HrSpriteNum", withExtendCls: false },
        // { clsName: "CMyCard", withExtendCls: false },
        // { clsName: "GCSliderLayer", withExtendCls: false, color: cc.color(255, 0, 0) },
        // { clsName: "ShareQRCodeLayer", withExtendCls: false, color: cc.color(255, 0, 0) },
    ];

    var org_node_addchild = cc.Node.prototype.addChild;
    cc.Node.prototype.addChild = function () {

        if(!cctool.PauseCreateStackLogTool) {
            var child = arguments[0];

            if (child) {
                var findedClass = false;
                for (var i = 0; i < targetClasses.length; i++) {
                    var targetClass = targetClasses[i];
                    if (targetClass["cls"] && cctool.checkClass(child, targetClass["cls"], targetClass["withExtendCls"])) {
                        findedClass = true;
                        break;
                    }
                    if (targetClass["clsName"] && cctool.checkClassByName(child, targetClass["clsName"], targetClass["withExtendCls"])) {
                        findedClass = true;
                        break;
                    }
                }
                if (findedClass)
                    cctool.showTargetCenter_mes(child, true, "加入流程：\n" + cctool.getStackLog(true), targetClass["color"]);
            }
        }

        return cctool.callFunctionWithArguments(org_node_addchild, this, arguments);
    };


})(cctool);

