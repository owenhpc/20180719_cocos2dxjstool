/**
 * Created by huangpinqian on 2018/8/6.
 * 攔截並印出所有方法的呼叫，啟動很慢，且不穩定！
 * 執行 enableCallMethodInfo() 方法啟動
 * new 的呼叫方式不知道怎麼攔截（方法在 ViewObjCatchData.js 中）
 * 目前只攔截宣告在 Class 裡的方法而已
 * 動態建立的方法還沒有攔截
 */




var cctool = cctool || {};
(function(module){
    module.DebugLog_CallMethodInfo = true;

    var startup = false;    //總開關

    module.enableProxyMethod = true;   //給外部控制的開關

    var pauseProxyMethod = true;  //成功啟動前都先暫停


    module.enableCallMethodInfo = function () {
        if(startup)
            return;
        startup = true;
        cc.log("準備運行 DebugLog_CallMethodInfo ！");

        pauseProxyMethod = true;  //成功啟動前都先暫停



        var nowProxyIdx = -1;


        //不能代理的方法（代理方法裡面用到的都不能重複代理！）
        // var ignoreFunc = [
        //     cc.p,
        //     cc.size,
        //     cc.color,
        //     cc.rect,
        //     cc.log,
        //     log,
        //     console.log,
        //     cctool.logStack,
        // ];
        // var ignoreFuncNames = ["Error", "Object", "callFunctionWithArguments"];
        var ignoreFuncNames = [
            "p",    //cc.p
            "size", //cc.size
            "color",//cc.color
            "rect", //cc.rect

            "enableCallMethodInfo",
            "callFunctionWithArguments",
            "showTargetCenter_mes",
            "getObjLog",
            "Array",
            "Boolean",
            "Data",
            "Number",
            "String",
            "RegExp",
            "InternalError",
            "Object",
            "Error",
            "Date",
            "ctor",
            "_ctor",
            "checkClass",
            "checkClassByName",
            "jsondecode",
            "isObject",
            "isFunction",
            "Packet",   //PBM.Packet
            "toString", //太多
            "isString", //太多
            // "parse",
            // "Parser",
        ];
        var ignoreObjNames = [
            "ParticleSystem",
            "ProtoBuf",
            "cctool",
        ];

        var checkedObj = [];
        var loopFunc_objField = function (obj, fieldName, stackLog, indent){
            if(!obj)
                return true;

            var isObj = false;
            var isFunc = false;

            try{
                isObj = cctool.checkIsClass(obj[fieldName]) || cctool.checkIsObj(obj[fieldName]);
                isFunc = cctool.checkIsFunc(obj[fieldName]);
            }catch (e){
                // cc.log("取值失敗：", fieldName);
                return true;
            }

            var indentStr = "";
            for(var ii=0; ii<indent; ii++){
                indentStr += "  ";
            }
            cc.log("尋訪變數：" + indentStr, fieldName, "\t", (isObj ? "isObj" : ""), (isFunc ? "isFunc" : ""));

            // if(cctool.checkIsFunc(obj[fieldName])){
            if(isFunc){
                //Function 物件，攔截！
                var targetFunc = obj[fieldName];

                if(targetFunc["__cctool_checkedFunc"])
                    return true; //已處理過

                targetFunc["__cctool_checkedFunc"] = true;   //標示已處理過

                if(ignoreFuncNames && ignoreFuncNames.length > 0){
                    for(var i=0; i<ignoreFuncNames.length; i++) {
                        if(ignoreFuncNames[i] === fieldName)
                            return true;
                    }
                }

                //代理

                nowProxyIdx++;

                // 找出攔截會出問題的方法
                // if(nowProxyIdx < 0 || nowProxyIdx > 1000)    //ok
                // if(nowProxyIdx < 1000 || nowProxyIdx > 1500)    //ok
                // if(nowProxyIdx < 1500 || nowProxyIdx > 2000)    //xx
                // if(nowProxyIdx < 1500 || nowProxyIdx > 1700)    //ok
                // if(nowProxyIdx < 1700 || nowProxyIdx > 1850)    //ok
                // if(nowProxyIdx < 1850 || nowProxyIdx > 1950)    //xx
                // if(nowProxyIdx < 1850 || nowProxyIdx > 1900)    //ok
                // if(nowProxyIdx < 1900 || nowProxyIdx > 1925)    //xx
                // if(nowProxyIdx < 1900 || nowProxyIdx > 1912)    //ok
                // if(nowProxyIdx < 1912 || nowProxyIdx > 1920)    //xx
                // if(nowProxyIdx < 1912 || nowProxyIdx > 1917)    //xx
                // if(nowProxyIdx < 1912 || nowProxyIdx > 1914)    //xx

                // if(nowProxyIdx < 0 || nowProxyIdx > 1913)    //ok
                // if(nowProxyIdx < 1913 || nowProxyIdx > 2000)    //
                //     return true;
                // if(fieldName !== "GetMenuItemSprite")
                //     return true;

                // stackLog += "." + fieldName;
                cc.log("代理方法：", nowProxyIdx, "\t", stackLog);


                obj[fieldName] = function(org_func){
                    return function () {
                        // if(cctool.pauseProxyMethod){
                        if(!cctool.enableProxyMethod || pauseProxyMethod){
                            return cctool.callFunctionWithArguments(org_func, this, arguments);

                        }else{
                            // cctool.pauseProxyMethod = true;
                            pauseProxyMethod = true;
                            // cc.log("攔截呼叫：", cctool.logStack());
                            cc.log("攔截呼叫：", stackLog, "\narguments：", cctool.getObjLog(arguments), "\nstack：\n", cctool.getStackLog());
                            // cctool.pauseProxyMethod = false;
                            pauseProxyMethod = false;
                            return cctool.callFunctionWithArguments(org_func, this, arguments);
                        }
                    };
                }(targetFunc);


                // var org_func = targetFunc;
                // obj[fieldName] = function () {
                //     if(pauseProxy){
                //         return cctool.callFunctionWithArguments(org_func, this, arguments);
                //
                //     }else{
                //         pauseProxy = true;
                //         // cc.log("攔截呼叫：", cctool.logStack());
                //         cc.log("攔截呼叫：", fieldName, "\narguments：", cctool.getObjLog(arguments), "\nstack：\n", cctool.getStackLog());
                //         pauseProxy = false;
                //         return cctool.callFunctionWithArguments(org_func, this, arguments);
                //     }
                // };
                return true;
            }

            // if(cc.isObject(obj[fieldName]) || obj[fieldName].prototype){
            if(isObj){
                //obj 物件，尋訪 obj 下變數
                var targetObj = obj[fieldName];

                if(checkedObj.indexOf(obj[fieldName]) !== -1)
                    return true; //已處理過
                checkedObj.push(obj[fieldName]);

                if(ignoreObjNames && ignoreObjNames.length > 0){
                    for(var i=0; i<ignoreObjNames.length; i++) {
                        if(ignoreObjNames[i] === fieldName)
                            return true;
                    }
                }

                // if(targetObj["__cctool_checkedFunc"])
                //     return; //已處理過
                //
                // targetObj["__cctool_checkedFunc"] = true;   //標示已處理過

                // cc.log("處理類別：", fieldName);

                // var targetObjFieldNames = [];
                cctool.loopAllPropertyNames(
                    targetObj,
                    function (loopFieldName) {
                        // targetObjFieldNames.push(loopFieldName);
                        if(targetObj)
                            return loopFunc_objField(targetObj, loopFieldName, stackLog + "." + loopFieldName, indent+1);
                        return true;
                    },
                    true,
                    true
                );
                // for(var iname=0; iname<targetObjFieldNames.length; iname++){
                //     loopFunc_objField(targetObj, targetObjFieldNames[iname]);
                // }
                return true;
            }

            // if(obj[fieldName].prototype){
            //     //Class 物件，尋訪 Class 下變數
            //     if(obj[fieldName].prototype["__cctool_checkedFunc"])
            //         return; //已處理過
            //
            //     obj[fieldName].prototype["__cctool_checkedFunc"] = true;   //標示已處理過
            //
            //     cc.log("處理類別：", fieldName);
            //
            //     cctool.loopAllPropertyNames(
            //         obj[fieldName].prototype,
            //         function (fieldName) {
            //             if(obj[fieldName])
            //                 loopFunc_objField(obj[fieldName].prototype, fieldName);
            //             return true;
            //         },
            //         true,
            //         true
            //     );
            //     return;
            // }
            return true;
        };


        cctool.loopAllPropertyNames(
            cctool.GlobalInst,
            function (fieldName) { return loopFunc_objField(cctool.GlobalInst, fieldName, "Global." + fieldName, 0); },
            true,
            false   //關掉，不然太多
        );

        cc.log("成功運行 DebugLog_CallMethodInfo ！");
        // cctool.pauseProxyMethod = false;
        pauseProxyMethod = false;

    };



})(cctool);


