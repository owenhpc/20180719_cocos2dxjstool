/**
 * Created by huangpinqian on 2018/7/20.
 * 攔截 "監聽事件" 動作
 */


// debug 用，設定幾次後開始忽略吃掉滑鼠事件，忽略後有些按鈕會出問題！
var clickTimer = 20;


var cctool = cctool || {};
(function(module) {
    module.EventListenerTool = true;


    //攔截事件監聽
    var org_cc_eventManager_addListener = cc.eventManager.addListener;
    cc.eventManager.addListener = function () {
        if(arguments.length > 0) {
            var listenData = arguments[0];
            var listener = arguments[1];
            if(listenData.event){
                if(listenData.event === cc.EventListener.KEYBOARD){
                    var className = cctool.tryGetClassName(listener);
                    if(className){
                        className = "{" + className + "}";
                    }else{
                        className = listener.toString();
                    }
                    cc.log("[owen] 發現註冊鍵盤監聽", "[" + cctool.GetInstNID(listener) + "]", className, listenData.onKeyReleased, "\n", listenerStack);


                    if(listenData.onKeyReleased){
                        var listenerStack = cctool.getStackLog(true);
                        var org_onKeyReleased = listenData.onKeyReleased;
                        listenData.onKeyReleased = function (keyCode, event) {
                            org_onKeyReleased.call(this, keyCode, event);
                            var className = cctool.tryGetClassName(listener);
                            if(className){
                                className = "{" + className + "}";
                            }else{
                                className = listener.toString();
                            }
                            cc.log("[owen] 發現觸發鍵盤事件的監聽：", "[" + cctool.GetInstNID(listener) + "]", className, "\n", listenerStack);
                        }
                    }
                }
            }

            if (listenData.onTouchBegan) {
                var listenerStack = cctool.getStackLog(true);
                var org_onTouchBegan = listenData.onTouchBegan;
                listenData.onTouchBegan = function (touch, event) {
                    var eatEvent = org_onTouchBegan.call(this, touch, event);
                    if(eatEvent){
                        var className = cctool.tryGetClassName(listener);
                        if(className){
                            className = "{" + className + "}";
                        }else{
                            className = listener.toString();
                        }
                        cc.log("[owen] 發現吃掉滑鼠點擊事件的監聽：", "[" + cctool.GetInstNID(listener) + "]", className, "\n", listenerStack);
                    }

                    // if(eatEvent) {
                    //     clickTimer--;
                    //     if (clickTimer <= 0){
                    //         cc.log("[Error] 原本吃掉事件的監聽強制取消吃掉！");
                    //         return false;
                    //     }else{
                    //         cc.log("[Error] 再 " + clickTimer + " 次強制取消吃掉！");
                    //     }
                    // }

                    return eatEvent;
                }
            }
        }

        return cctool.callFunctionWithArguments(org_cc_eventManager_addListener, this, arguments);
    };

    var checkAddedToScene = function (target) {
        if(target === null)
            return false;

        var checkParent = target;
        while(checkParent !== null){
            if(cctool.checkClass(checkParent, cc.Scene, true))
                return true;
            if(checkParent === null)
                return false;
            checkParent = checkParent.parent;
        }
        return true;
    };

    var org_node_schedule = cc.Node.prototype.schedule;
    cc.Node.prototype.schedule = function () {
        var stackLog = cctool.getStackLog();
        this.retain();
        var org_func = arguments[0];
        var sec = arguments.length > 1 ? arguments[1] : -1;

        if(!org_func){
            cc.log("[Error] 註冊 schedule 不存在方法：" + cctool.tryGetClassName(this), sec, stackLog);
            return cctool.callFunctionWithArguments(org_node_schedule, this, arguments);
        }

        // cc.log("註冊 schedule：" + cctool.tryGetClassName(this), sec, stackLog);

        org_func._proxyScheduleFunc = function () {
            if(!checkAddedToScene(this))
                cc.log("[Error] 出現應該被釋放的 schedule 被呼叫！", cctool.tryGetClassName(this) + ", " + cctool.getParentsLog(this) + "\nStack:\n" + stackLog);
            return cctool.callFunctionWithArguments(org_func, this, arguments);
        };
        arguments[0] = org_func._proxyScheduleFunc;
        if(!this._haveRetainOnAddSchedule) {
            this._haveRetainOnAddSchedule = 1;
        }else {
            this._haveRetainOnAddSchedule++;
        }

        return cctool.callFunctionWithArguments(org_node_schedule, this, arguments);
    };

    var org_node_unschedule = cc.Node.prototype.unschedule;
    cc.Node.prototype.unschedule = function () {
        var stackLog = cctool.getStackLog();
        var org_func = arguments[0];
        if(!org_func){
            cc.log("[Error] 取消 schedule 不存在方法：" + cctool.tryGetClassName(this), stackLog);
            return cctool.callFunctionWithArguments(org_node_unschedule, this, arguments);
            return;
        }

        if(this._haveRetainOnAddSchedule && this._haveRetainOnAddSchedule > 0) {
            this._haveRetainOnAddSchedule -- ;
            this.release();
        }

        // cc.log("取消 schedule：" + cctool.tryGetClassName(this), stackLog);

        arguments[0] = org_func._proxyScheduleFunc;
        org_func._proxyScheduleFunc = null;

        return cctool.callFunctionWithArguments(org_node_unschedule, this, arguments);
    };


    var org_node_scheduleUpdate = cc.Node.prototype.scheduleUpdate;
    cc.Node.prototype.scheduleUpdate = function () {
        this.retain();
        var org_func = this.update;
        var stackLog = cctool.getStackLog();
        cc.log("註冊 scheduleUpdate：" + cctool.tryGetClassName(this), stackLog);
        if(!this._haveRetainOnAddScheduleUpdate) {
            this._haveRetainOnAddScheduleUpdate = 1;
        }else {
            this._haveRetainOnAddScheduleUpdate++;
        }
        this.update = function () {
            if(!checkAddedToScene(this))
                cc.log("[Error] 出現應該被釋放的 scheduleUpdate 被呼叫！", cctool.tryGetClassName(this) + ", " + cctool.getParentsLog(this) + "\nStack:\n" + stackLog);
            return cctool.callFunctionWithArguments(org_func, this, arguments);
        };


        return cctool.callFunctionWithArguments(org_node_scheduleUpdate, this, arguments);
    };

    var org_node_unscheduleUpdate = cc.Node.prototype.unscheduleUpdate;
    cc.Node.prototype.unscheduleUpdate = function () {

        if(this._haveRetainOnAddScheduleUpdate && this._haveRetainOnAddScheduleUpdate > 0) {
            this._haveRetainOnAddScheduleUpdate--;
            this.release();
        }

        var stackLog = cctool.getStackLog();
        cc.log("取消 scheduleUpdate：" + cctool.tryGetClassName(this), stackLog);

        return cctool.callFunctionWithArguments(org_node_unscheduleUpdate, this, arguments);
    };

})(cctool);
