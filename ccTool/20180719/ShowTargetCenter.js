/**
 * Created by huangpinqian on 2018/7/17.
 * 顯示指定物件中心點與拖移定位工具
 * 並在點擊後顯示相關資訊
 * 可過濾 "物件資訊" 來看
 */


var cctool = cctool || {};
(function(module) {
    module.ShowTargetCenter = true;

    module.ShowInstNID = false;  //顯示 nid 值

    /** 顯示中心點位置 **/
    function showTargetCenter_mes(target, mouseMove, mes, color = null) {
        showTargetCenter(target, mouseMove, color ? color : cc.color(255, 255, 255, 255), cc.p(0, 0), 2, mes);
    }
    module.showTargetCenter_mes = showTargetCenter_mes;

    /** 顯示中心點位置 **/
    function showTargetCenter(target, mouseMove, color, offset, lineWidth, mes) {
        try {
            _showTargetCenter(target, mouseMove, color, offset, lineWidth, mes);
        } catch (e) {
            cc.log("Error showTargetCenter 失敗：" + e.message + "\nErrorStack：\n" + cctool.toShortPath(e.stack));
        }
    }
    module.showTargetCenter = showTargetCenter;


    function createLayerColor(parent, name, color, anchorX, anchorY, depth) {
        var inst = new cc.LayerColor(color);

        if (inst.setIgnoreAnchorPointForPosition) {
            inst.setIgnoreAnchorPointForPosition(false);
        } else if (inst.ignoreAnchorPointForPosition) {
            inst.ignoreAnchorPointForPosition(false);
        }

        inst.setAnchorPoint(anchorX, anchorY);
        inst.name = name;
        cctool.PauseCreateStackLogTool = true;
        parent.addChild(inst, depth);
        cctool.PauseCreateStackLogTool = false;

        return inst;
    }

    //建立雙色線
    function createLayerColorLine(parent, name, length, width, rotation, posX, posY, depth) {
        //黑線
        var line = createLayerColor(parent, name + "_A", cc.color(0, 0, 0, 255), 0.5, 1, depth);
        line.setContentSize(cc.size(length, width/2));
        line.setRotation(rotation);
        line.x = posX;
        line.y = posY + width/2;
        //白線（加在黑線下）
        line = createLayerColor(line, name + "_B", cc.color(255, 255, 255, 255), 0.5, 0, depth);
        line.setContentSize(cc.size(length, width/2));
        line.x = length/2;
        line.y = width/2;
    }

    function getAnchorPointPosition(target, offset) {

        var showPoint = null;
        if ((target.isIgnoreAnchorPointForPosition && target.isIgnoreAnchorPointForPosition()) || !target.getAnchorPoint) {
            if(offset){
                showPoint = offset;
            }else{
                showPoint = cc.p(0, 0);
            }

        } else {
            var tarSize = cc.p(0, 0);
            if (target.getSize) {
                tarSize = target.getSize();
            } else if (target.getContentSize) {
                tarSize = target.getContentSize();
            }
            if(offset){
                showPoint = cc.p(tarSize.width * target.getAnchorPoint().x + offset.x, tarSize.height * target.getAnchorPoint().y + offset.y);
            }else{
                showPoint = cc.p(tarSize.width * target.getAnchorPoint().x, tarSize.height * target.getAnchorPoint().y);
            }
        }
        return showPoint;
    }

    var firstTime = true;   //第一次顯示中心點位置

    /**
     * 判斷物件是否有建立準心
     * @param target
     * @constructor
     */
    module.CheckHaveConcentric = function (target) {
        if(!target)
            return false;
        return "__coverLayerC" in target;
    };

    /** 顯示中心點位置 **/
    function _showTargetCenter(target, mouseMove, color, offset, lineWidth, mes) {
        if(firstTime){
            if(cctool.DebugLog_Hierarchy){
                //考慮用一個容器包起來
                cctool.HierarchyIgnoreNames.push("__cctool_nidLabel");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerA");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerB");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerC");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerD");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerE");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerE.1_A");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerE.1_B");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerE.2_A");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerE.2_B");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerF");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerF.1_A");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerF.1_B");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerF.2_A");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerF.2_B");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerF.3_A");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerF.3_B");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerF.4_A");
                cctool.HierarchyIgnoreNames.push("__cctool_coverLayerF.4_B");
            }
        }
        firstTime = false;

        if (mouseMove === undefined) {
            mouseMove = true;
        }
        color = color || cc.color(255, 255, 255, 255);
        offset = offset || cc.p(0, 0);
        lineWidth = lineWidth || 2;

        if (!target)
            return;

        if (!target.addChild) {
            cc.log("此物件無法加子物件！無法顯示中心點：", typeof(target));
            return;
        }




        //訊息儲存
        if (!target.__showTargetCenter_mes)
            target.__showTargetCenter_mes = [];
        if (mes) {
            target.__showTargetCenter_mes.push(mes);
        } else {
            target.__showTargetCenter_mes.push("no mes");
        }



        var lineLength = 60;   //準心十字長度

        //計算準心放置位置（設定在目標物件的 anchor point 位置，而不是 0, 0 位置）
        var showPoint = getAnchorPointPosition(target, offset);

        // 顯示 nid
        if(cctool.ShowInstNID){
            if(!target.__nidLabel){
                target.__nidLabel = new cc.LabelTTF(cctool.GetInstNID(target).toString(), "Arial", 20, cc.TEXT_ALIGNMENT_LEFT);
                target.__nidLabel.setAnchorPoint(0, 0);
                target.__nidLabel.setPosition(showPoint.x + 2, showPoint.y + 2);
                target.__nidLabel.setColor(color);
                target.__nidLabel.name = "__cctool_nidLabel";
                cctool.PauseCreateStackLogTool = true;
                target.addChild(target.__nidLabel);
                cctool.PauseCreateStackLogTool = false;
            }
        }

        // 建立或更新準心物件（不重複建立）
        if (!target.__coverLayerA)    //準心直線
            target.__coverLayerA = createLayerColor(target, "__cctool_coverLayerA", color, 0.5, 0.5, 100000);
        target.__coverLayerA.setContentSize(cc.size(lineWidth, lineLength));
        target.__coverLayerA.setColor(color);
        target.__coverLayerA.setPosition(showPoint);

        if (!target.__coverLayerB)    //準心橫線
            target.__coverLayerB = createLayerColor(target, "__cctool_coverLayerB", color, 0.5, 0.5, 100000);
        target.__coverLayerB.setContentSize(cc.size(lineLength, lineWidth));
        target.__coverLayerB.setColor(color);
        target.__coverLayerB.setPosition(showPoint);

        //建立準心方形底並處理滑鼠監聽
        if (mouseMove) {
            if (!target.__coverLayerC) {    //方形底
                target.__coverLayerC = createLayerColor(target, "__cctool_coverLayerC", cc.color(color.r, color.g, color.b, 100), 0.5, 0.5, 100100);
                // target.__coverLayerC.setContentSize(30, 30);
                target.__coverLayerC.setContentSize(lineLength/3, lineLength/3);
                target.__coverLayerC.setRotation(45);
                target.__coverLayerC.mousePos = null;
                target.__coverLayerC.onStrMovePos = null;

                var getPosLogStr = function (floatVal, addDot) {
                    var log = (Math.floor(floatVal * 100) / 100).toString();
                    if (addDot)
                        log += ",";
                    while (log.length < 10) {
                        log += " ";
                    }
                    return log;
                };


                if (target.getContentSize) {
                    if(!target.__coverLayerD) { //滑鼠按住時顯示的物件範圍
                        target.__coverLayerD = createLayerColor(target, "__cctool_coverLayerD", cc.color(color.r, color.g, color.b, 50), 0, 0, 100000);
                        target.__coverLayerD.setVisible(false);
                    }

                    if(!target.__coverLayerE) { //滑鼠按住時顯示的當前物件 anchorPoint 標示
                        var lineLengthE = 20;
                        var lineWidthE = 4;
                        target.__coverLayerE = new cc.Node();
                        target.__coverLayerE.setVisible(false);
                        target.__coverLayerE.name = "__cctool_coverLayerE";
                        cctool.PauseCreateStackLogTool = true;
                        target.addChild(target.__coverLayerE, 100000);
                        cctool.PauseCreateStackLogTool = false;
                        createLayerColorLine(target.__coverLayerE, "__cctool_coverLayerE.1", lineLengthE, lineWidthE, 45, 0, 0, 100020);
                        createLayerColorLine(target.__coverLayerE, "__cctool_coverLayerE.2", lineLengthE, lineWidthE, -45, 0, 0, 100020);
                    }
                }


                var listener = cc.EventListener.create({
                    event: cc.EventListener.TOUCH_ONE_BY_ONE,
                    swallowTouches: true,
                    onTouchBegan: function (touch, event) {
                        var pos = touch.getLocation();
                        if (!cc.rectContainsPoint(target.__coverLayerC.getBoundingBoxToWorld(), pos))
                            return false;

                        cctool.logStack("[owen] 點擊事件！！");

                        target.__coverLayerC.mousePos = null;

                        target.__coverLayerC.onStrMovePos = pos;
                        var tarPos = target.getPosition();
                        var posX = getPosLogStr(tarPos.x, true);
                        var posY = getPosLogStr(tarPos.y, false);

                        cctool.LogView("點擊：", target);

                        cc.log("MoveBegin:", posX + posY);
                        target.__coverLayerC.mousePos = pos;


                        if(target.__coverLayerD) {
                            var anchorPosition = getAnchorPointPosition(target);
                            target.__coverLayerD.setAnchorPoint(target.getAnchorPoint());
                            target.__coverLayerD.setPosition(anchorPosition);
                            target.__coverLayerD.setContentSize(target.getContentSize().width, target.getContentSize().height);
                            target.__coverLayerD.setVisible(true);
                        }

                        if(target.__coverLayerE){
                            target.__coverLayerE.setPosition(anchorPosition);
                            target.__coverLayerE.setVisible(true);
                        }

                        //放在跟 target 同一層下的原點位置（每次重建）
                        if(target.parent && !cctool.checkClass(target.parent, cc.Menu)) {
                            target.__coverLayerF = new cc.Node();
                            target.__coverLayerF.name = "__cctool_coverLayerF";
                            cctool.PauseCreateStackLogTool = true;
                            target.parent.addChild(target.__coverLayerF, 100000);
                            cctool.PauseCreateStackLogTool = false;
                            var lineLengthF = 20;
                            var lineWidthF = 4;
                            createLayerColorLine(target.__coverLayerF, "__cctool_coverLayerF.1", lineLengthF+lineWidthF, lineWidthF, 0, 0, lineLengthF / 2, 100020);
                            createLayerColorLine(target.__coverLayerF, "__cctool_coverLayerF.2", lineLengthF+lineWidthF, lineWidthF, 90, lineLengthF / 2, 0, 100020);
                            createLayerColorLine(target.__coverLayerF, "__cctool_coverLayerF.3", lineLengthF+lineWidthF, lineWidthF, 180, 0, -lineLengthF / 2, 100020);
                            createLayerColorLine(target.__coverLayerF, "__cctool_coverLayerF.4", lineLengthF+lineWidthF, lineWidthF, 270, -lineLengthF / 2, 0, 100020);
                        }

                        return true;    //有點到此工具就吃掉點擊事件
                    },
                    onTouchMoved: function (touch, event) {
                        if (target.__coverLayerC.mousePos === null) {
                            return;
                        }

                        var pos = touch.getLocation();
                        var tarPos = target.getPosition();
                        tarPos.x += pos.x - target.__coverLayerC.mousePos.x;
                        tarPos.y += pos.y - target.__coverLayerC.mousePos.y;
                        target.setPosition(tarPos);
                        target.__coverLayerC.mousePos = pos;

                        var posX = getPosLogStr(tarPos.x, true);
                        var posY = getPosLogStr(tarPos.y, false);
                        var dx = getPosLogStr(pos.x - target.__coverLayerC.onStrMovePos.x, true);
                        var dy = getPosLogStr(pos.y - target.__coverLayerC.onStrMovePos.y, false);

                        cc.log("Moveing:", posX + posY, "當次拖曳差距:", dx + dy);
                    },
                    onTouchEnded: function (touch, event) {
                        target.__coverLayerC.mousePos = null;

                        if(target.__coverLayerD)
                            target.__coverLayerD.setVisible(false);

                        if(target.__coverLayerE)
                            target.__coverLayerE.setVisible(false);

                        if(target.__coverLayerF){
                            target.__coverLayerF.removeFromParent();
                            target.__coverLayerF = null;
                        }

                    }
                });
                cc.eventManager.addListener(listener, target.__coverLayerC);
            }
            target.__coverLayerC.setColor(cc.color(color.r, color.g, color.b, 100));
            target.__coverLayerC.setPosition(showPoint);

        } else {
            if (target.__coverLayerC) {
                target.__coverLayerC.removeFromParent();
                delete target["__coverLayerC"];
            }
        }
    }

    /** 被移除時的通知 **/
    if(cc.Node.prototype.removeChild){
        var org_node_removeChild = cc.Node.prototype.removeChild;
        cc.Node.prototype.removeChild = function () {
            var child = arguments[0];

            if (child) {
                if(child.parent.__coverLayerA && child.parent.__coverLayerA === child) {
                    cctool.logStack("[ShowTargetCenter] 標示已被移除（removeChild）！");
                    delete this.__coverLayerA;
                    delete this.__coverLayerB;
                    delete this.__coverLayerC;
                    delete this.__coverLayerD;
                    delete this.__coverLayerE;
                    delete this.__coverLayerF;
                }
            }

            return cctool.callFunctionWithArguments(org_node_removeChild, this, arguments);
        };
    }

    if(cc.Node.prototype.removeAllChildren) {
        var org_node_removeAllChildren = cc.Node.prototype.removeAllChildren;
        cc.Node.prototype.removeAllChildren = function () {

            if (this.__coverLayerA) {
                cctool.logStack("[ShowTargetCenter] 標示已被移除（removeAllChildren）！");
                delete this.__coverLayerA;
                delete this.__coverLayerB;
                delete this.__coverLayerC;
                delete this.__coverLayerD;
                delete this.__coverLayerE;
                delete this.__coverLayerF;
            }

            return cctool.callFunctionWithArguments(org_node_removeAllChildren, this, arguments);
        };
    }


    /** 記錄下 text 設定內文資料（點擊準心後顯示） ** /
     var org__LabelTTF_setString = cc.LabelTTF.prototype.setString;
     cc.LabelTTF.prototype.setString = function () {
            var str = arguments[0];
            cctool.showTargetCenter_mes(this, true, "設定內文 LabelTTF：" + str + "\n" + cctool.getStackLog());
            return cctool.callFunctionWithArguments(org__LabelTTF_setString, this, arguments);
        };
     var org__LabelAtlas_setString = cc.LabelAtlas.prototype.setString;
     cc.LabelAtlas.prototype.setString = function () {
            var str = arguments[0];
            cctool.showTargetCenter_mes(this, true, "設定內文 LabelAtlas：" + str + "\n" + cctool.getStackLog());
            return cctool.callFunctionWithArguments(org__LabelAtlas_setString, this, arguments);
        };
     //*/

    /** 設定座標 Log，會太多，要過濾 ** /
     var org__cc_Node_setPoition = cc.Node.prototype.setPosition;
     cc.Node.prototype.setPosition = function () {
            if(arguments.length === 1){
                var pos = arguments[0];
                cctool.showTargetCenter_mes(this, true, "設定座標：" + pos.x + ", " + pos.y + "\nStack：" + cctool.getStackLog());
            }else if(arguments.length > 1){
                var x = arguments[0];
                var y = arguments[1];
                cctool.showTargetCenter_mes(this, true, "設定座標：" + x + ", " + y + "\nStack：" + cctool.getStackLog());
            }

           return cctool.callFunctionWithArguments(org__cc_Node_setPoition, this, arguments);
        };
     //*/



})(cctool);




// 過濾 "物件訊息" 實例：
// JS: /*-13---*/	點擊： 物件訊息：[1] {LabelTTF} "START"
// JS: /*-17---*/	點擊： 物件訊息：[9] {Sprite} "src/DiLei/res/common_frame_05.png"
// JS: /*-21---*/	點擊： 物件訊息：[324] {Sprite} "src/DiLei/res/common_frame_05.png"
// JS: /*-25---*/	點擊： 物件訊息：[671] {Sprite} "src/DiLei/res/brzjh_dot.png"





// 單一點擊 Log 實例（過濾 "-25-" 字串）：
// JS: /*-25---*/	點擊： 物件訊息：[671] {Sprite} "src/DiLei/res/brzjh_dot.png"
// /*-25-*/			[準心顏色]：255,255,255, alpha: 255
// /*-25-*/			[物件顏色]：255,255,255, alpha: 255
// /*-25-*/			[Position]：48.599998474121094, 48.599998474121094
// /*-25-*/			[Scale]：3.471428632736206, 3.471428632736206
// /*-25-*/			[Anchor]：0.5, 0.5
// /*-25-*/			[CostonSize]：14, 14
// /*-25-*/			[父物件表]：[3] {DiLeiGameScene} / [326] {DiLeiGameLayer} / [508] {DiLeiBox} / <Target>
// /*-25-*/			[點擊訊息0]：加入流程：
// /*-25-*/		cc.Node.prototype.addChild@/Users/.../DiLei iOS.app/src/ccTool/20180719/CreateStackLogTool.js:75
// /*-25-*/		DiLeiBox<.setSafeTag@/Users/.../DiLei iOS.app/src/DiLei/DiLeiBox.js:127
// /*-25-*/		DiLeiGameLayer<.onClickBox@/Users/.../DiLei iOS.app/src/DiLei/DiLeiGameLayer.js:116
// /*-25-*/		GameEventNotifier<.addListener/listenFunc@/Users/.../DiLei iOS.app/src/DiLei/com/GameEventNotifier.js:50
// /*-25-*/		cc.eventManager.dispatchCustomEvent@/Users/.../DiLei iOS.app/script/jsb_cocos2d.js:1652
// /*-25-*/		GameEventNotifier<.dispatchEvent@/Users/.../DiLei iOS.app/src/DiLei/com/GameEventNotifier.js:99
// /*-25-*/		DiLeiBox<.onGetMouseEvent@/Users/.../DiLei iOS.app/src/DiLei/DiLeiBox.js:109
// /*-25-*/
// /*-25-*/			[當前配置]：[Hierarchy] DiLeiGameScene :
// /*-25-*/		 < @: 正常顯示 >
// /*-25-*/		 < X: 設定為隱藏 >
// /*-25-*/		 < O: 設定為顯示，但父物件隱藏 >
// /*-25-*/		 < +: 有建立準心，有開啟 ShowTargetCenter 套件才有 >
// /*-25-*/		[3]  @ {DiLeiGameScene}
// /*-25-*/			[4]  @ {Camera}
// /*-25-*/			[326]  @ {DiLeiGameLayer}
// /*-25-*/				[327] +@ {Sprite} "src/DiLei/res/bombmap.png"
// /*-25-*/				[328]  @ {DiLeiBox}
// /*-25-*/					[329]  @ {Button}
// /*-25-*/				[330]  @ {DiLeiBox}
// /*-25-*/					[331]  @ {Button}
// /*-25-*/				[332]  @ {DiLeiBox}
// /*-25-*/					[333]  @ {Button}
// /*-25-*/					[334] +@ {LabelTTF} "1"
// /*-25-*/						[335]  @ {Label} "1"
// /*-25-*/							[336]  @ {Sprite}
// /*-25-*/				[337]  @ {DiLeiBox}
// /*-25-*/					[338]  @ {Button}
// /*-25-*/					[339] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[340]  @ {DiLeiBox}
// /*-25-*/					[341]  @ {Button}
// /*-25-*/					[342] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[343]  @ {DiLeiBox}
// /*-25-*/					[344]  @ {Button}
// /*-25-*/					[345] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[346]  @ {DiLeiBox}
// /*-25-*/					[347]  @ {Button}
// /*-25-*/					[348] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[349]  @ {DiLeiBox}
// /*-25-*/					[350]  @ {Button}
// /*-25-*/					[351] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[352]  @ {DiLeiBox}
// /*-25-*/					[353]  @ {Button}
// /*-25-*/					[354] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[355]  @ {DiLeiBox}
// /*-25-*/					[356]  @ {Button}
// /*-25-*/					[357] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[358]  @ {DiLeiBox}
// /*-25-*/					[359]  @ {Button}
// /*-25-*/				[360]  @ {DiLeiBox}
// /*-25-*/					[361]  @ {Button}
// /*-25-*/				[362]  @ {DiLeiBox}
// /*-25-*/					[363]  @ {Button}
// /*-25-*/					[364] +@ {LabelTTF} "1"
// /*-25-*/						[365]  @ {Label} "1"
// /*-25-*/							[366]  @ {Sprite}
// /*-25-*/				[367]  @ {DiLeiBox}
// /*-25-*/					[368]  @ {Button}
// /*-25-*/					[369] +@ {LabelTTF} "1"
// /*-25-*/						[370]  @ {Label} "1"
// /*-25-*/							[371]  @ {Sprite}
// /*-25-*/				[372]  @ {DiLeiBox}
// /*-25-*/					[373]  @ {Button}
// /*-25-*/					[374] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[375]  @ {DiLeiBox}
// /*-25-*/					[376]  @ {Button}
// /*-25-*/					[377] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[378]  @ {DiLeiBox}
// /*-25-*/					[379]  @ {Button}
// /*-25-*/					[380] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[381]  @ {DiLeiBox}
// /*-25-*/					[382]  @ {Button}
// /*-25-*/					[383] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[384]  @ {DiLeiBox}
// /*-25-*/					[385]  @ {Button}
// /*-25-*/					[386] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[387]  @ {DiLeiBox}
// /*-25-*/					[388]  @ {Button}
// /*-25-*/					[389] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[390]  @ {DiLeiBox}
// /*-25-*/					[391]  @ {Button}
// /*-25-*/					[392] +@ {LabelTTF} "1"
// /*-25-*/						[393]  @ {Label} "1"
// /*-25-*/							[394]  @ {Sprite}
// /*-25-*/				[395]  @ {DiLeiBox}
// /*-25-*/					[396]  @ {Button}
// /*-25-*/					[397] +@ {LabelTTF} "1"
// /*-25-*/						[398]  @ {Label} "1"
// /*-25-*/							[399]  @ {Sprite}
// /*-25-*/				[400]  @ {DiLeiBox}
// /*-25-*/					[401]  @ {Button}
// /*-25-*/				[402]  @ {DiLeiBox}
// /*-25-*/					[403]  @ {Button}
// /*-25-*/					[404] +@ {LabelTTF} "1"
// /*-25-*/						[405]  @ {Label} "1"
// /*-25-*/							[406]  @ {Sprite}
// /*-25-*/				[407]  @ {DiLeiBox}
// /*-25-*/					[408]  @ {Button}
// /*-25-*/					[409] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[410]  @ {DiLeiBox}
// /*-25-*/					[411]  @ {Button}
// /*-25-*/					[412] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[413]  @ {DiLeiBox}
// /*-25-*/					[414]  @ {Button}
// /*-25-*/					[415] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[416]  @ {DiLeiBox}
// /*-25-*/					[417]  @ {Button}
// /*-25-*/					[418] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[419]  @ {DiLeiBox}
// /*-25-*/					[420]  @ {Button}
// /*-25-*/					[421] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[422]  @ {DiLeiBox}
// /*-25-*/					[423]  @ {Button}
// /*-25-*/					[424] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[425]  @ {DiLeiBox}
// /*-25-*/					[426]  @ {Button}
// /*-25-*/					[427] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[428]  @ {DiLeiBox}
// /*-25-*/					[429]  @ {Button}
// /*-25-*/					[430] +@ {LabelTTF} "1"
// /*-25-*/						[431]  @ {Label} "1"
// /*-25-*/							[432]  @ {Sprite}
// /*-25-*/				[433]  @ {DiLeiBox}
// /*-25-*/					[434]  @ {Button}
// /*-25-*/				[435]  @ {DiLeiBox}
// /*-25-*/					[436]  @ {Button}
// /*-25-*/					[437] +@ {LabelTTF} "1"
// /*-25-*/						[438]  @ {Label} "1"
// /*-25-*/							[439]  @ {Sprite}
// /*-25-*/				[440]  @ {DiLeiBox}
// /*-25-*/					[441]  @ {Button}
// /*-25-*/					[442] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[443]  @ {DiLeiBox}
// /*-25-*/					[444]  @ {Button}
// /*-25-*/					[445] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[446]  @ {DiLeiBox}
// /*-25-*/					[447]  @ {Button}
// /*-25-*/					[448] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[449]  @ {DiLeiBox}
// /*-25-*/					[450]  @ {Button}
// /*-25-*/					[451] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[452]  @ {DiLeiBox}
// /*-25-*/					[453]  @ {Button}
// /*-25-*/					[454] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[455]  @ {DiLeiBox}
// /*-25-*/					[456]  @ {Button}
// /*-25-*/					[457] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[458]  @ {DiLeiBox}
// /*-25-*/					[459]  @ {Button}
// /*-25-*/					[460] +@ {LabelTTF} "1"
// /*-25-*/						[461]  @ {Label} "1"
// /*-25-*/							[462]  @ {Sprite}
// /*-25-*/				[463]  @ {DiLeiBox}
// /*-25-*/					[464]  @ {Button}
// /*-25-*/					[465] +@ {LabelTTF} "1"
// /*-25-*/						[466]  @ {Label} "1"
// /*-25-*/							[467]  @ {Sprite}
// /*-25-*/				[468]  @ {DiLeiBox}
// /*-25-*/					[469]  @ {Button}
// /*-25-*/				[470]  @ {DiLeiBox}
// /*-25-*/					[471]  @ {Button}
// /*-25-*/					[472] +@ {LabelTTF} "1"
// /*-25-*/						[473]  @ {Label} "1"
// /*-25-*/							[474]  @ {Sprite}
// /*-25-*/				[475]  @ {DiLeiBox}
// /*-25-*/					[476]  @ {Button}
// /*-25-*/					[477] +@ {LabelTTF} "2"
// /*-25-*/						[478]  @ {Label} "2"
// /*-25-*/							[479]  @ {Sprite}
// /*-25-*/				[480]  @ {DiLeiBox}
// /*-25-*/					[481]  @ {Button}
// /*-25-*/					[482] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[483]  @ {DiLeiBox}
// /*-25-*/					[484]  @ {Button}
// /*-25-*/					[485] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[486]  @ {DiLeiBox}
// /*-25-*/					[487]  @ {Button}
// /*-25-*/					[488] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[489]  @ {DiLeiBox}
// /*-25-*/					[490]  @ {Button}
// /*-25-*/					[491] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[492]  @ {DiLeiBox}
// /*-25-*/					[493]  @ {Button}
// /*-25-*/					[494] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[495]  @ {DiLeiBox}
// /*-25-*/					[496]  @ {Button}
// /*-25-*/				[497]  @ {DiLeiBox}
// /*-25-*/					[498]  @ {Button}
// /*-25-*/				[499]  @ {DiLeiBox}
// /*-25-*/					[500]  @ {Button}
// /*-25-*/				[501]  @ {DiLeiBox}
// /*-25-*/					[502]  @ {Button}
// /*-25-*/				[503]  @ {DiLeiBox}
// /*-25-*/					[504]  @ {Button}
// /*-25-*/					[505] +@ {LabelTTF} "1"
// /*-25-*/						[506]  @ {Label} "1"
// /*-25-*/							[507]  @ {Sprite}
// /*-25-*/				[508]  @ {DiLeiBox}
// /*-25-*/					[509]  @ {Button}
// /*-25-*/					[510] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/					[671] +@ {Sprite} "src/DiLei/res/brzjh_dot.png"  <----------------
// /*-25-*/				[511]  @ {DiLeiBox}
// /*-25-*/					[512]  @ {Button}
// /*-25-*/					[513] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[514]  @ {DiLeiBox}
// /*-25-*/					[515]  @ {Button}
// /*-25-*/					[516] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[517]  @ {DiLeiBox}
// /*-25-*/					[518]  @ {Button}
// /*-25-*/					[519] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[520]  @ {DiLeiBox}
// /*-25-*/					[521]  @ {Button}
// /*-25-*/					[522] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[523]  @ {DiLeiBox}
// /*-25-*/					[524]  @ {Button}
// /*-25-*/				[525]  @ {DiLeiBox}
// /*-25-*/					[526]  @ {Button}
// /*-25-*/				[527]  @ {DiLeiBox}
// /*-25-*/					[528]  @ {Button}
// /*-25-*/				[529]  @ {DiLeiBox}
// /*-25-*/					[530]  @ {Button}
// /*-25-*/					[531] +@ {LabelTTF} "1"
// /*-25-*/						[532]  @ {Label} "1"
// /*-25-*/							[533]  @ {Sprite}
// /*-25-*/				[534]  @ {DiLeiBox}
// /*-25-*/					[535]  @ {Button}
// /*-25-*/					[536] +@ {LabelTTF} "2"
// /*-25-*/						[537]  @ {Label} "2"
// /*-25-*/							[538]  @ {Sprite}
// /*-25-*/				[539]  @ {DiLeiBox}
// /*-25-*/					[540]  @ {Button}
// /*-25-*/					[541] +@ {LabelTTF} "2"
// /*-25-*/						[542]  @ {Label} "2"
// /*-25-*/							[543]  @ {Sprite}
// /*-25-*/				[544]  @ {DiLeiBox}
// /*-25-*/					[545]  @ {Button}
// /*-25-*/					[546] +@ {LabelTTF} "1"
// /*-25-*/						[547]  @ {Label} "1"
// /*-25-*/							[548]  @ {Sprite}
// /*-25-*/				[549]  @ {DiLeiBox}
// /*-25-*/					[550]  @ {Button}
// /*-25-*/					[551] +@ {LabelTTF} "1"
// /*-25-*/						[552]  @ {Label} "1"
// /*-25-*/							[553]  @ {Sprite}
// /*-25-*/				[554]  @ {DiLeiBox}
// /*-25-*/					[555]  @ {Button}
// /*-25-*/					[556] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[557]  @ {DiLeiBox}
// /*-25-*/					[558]  @ {Button}
// /*-25-*/					[559] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[560]  @ {DiLeiBox}
// /*-25-*/					[561]  @ {Button}
// /*-25-*/				[562]  @ {DiLeiBox}
// /*-25-*/					[563]  @ {Button}
// /*-25-*/				[564]  @ {DiLeiBox}
// /*-25-*/					[565]  @ {Button}
// /*-25-*/				[566]  @ {DiLeiBox}
// /*-25-*/					[567]  @ {Button}
// /*-25-*/					[568] +@ {LabelTTF} "1"
// /*-25-*/						[569]  @ {Label} "1"
// /*-25-*/							[570]  @ {Sprite}
// /*-25-*/				[325]  @ {DiLeiBox}
// /*-25-*/					[571]  @ {Button}
// /*-25-*/					[324] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/					[672] +@ {Sprite} "src/DiLei/res/brzjh_dot.png"
// /*-25-*/				[572]  @ {DiLeiBox}
// /*-25-*/					[573]  @ {Button}
// /*-25-*/					[574] +@ {LabelTTF} "1"
// /*-25-*/						[575]  @ {Label} "1"
// /*-25-*/							[576]  @ {Sprite}
// /*-25-*/				[577]  @ {DiLeiBox}
// /*-25-*/					[578]  @ {Button}
// /*-25-*/				[579]  @ {DiLeiBox}
// /*-25-*/					[580]  @ {Button}
// /*-25-*/					[581] +@ {LabelTTF} "1"
// /*-25-*/						[582]  @ {Label} "1"
// /*-25-*/							[583]  @ {Sprite}
// /*-25-*/				[584]  @ {DiLeiBox}
// /*-25-*/					[585]  @ {Button}
// /*-25-*/					[586] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[587]  @ {DiLeiBox}
// /*-25-*/					[588]  @ {Button}
// /*-25-*/					[589] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[590]  @ {DiLeiBox}
// /*-25-*/					[591]  @ {Button}
// /*-25-*/					[592] +@ {LabelTTF} "1"
// /*-25-*/						[593]  @ {Label} "1"
// /*-25-*/							[594]  @ {Sprite}
// /*-25-*/				[595]  @ {DiLeiBox}
// /*-25-*/					[596]  @ {Button}
// /*-25-*/					[597] +@ {LabelTTF} "1"
// /*-25-*/						[598]  @ {Label} "1"
// /*-25-*/							[599]  @ {Sprite}
// /*-25-*/				[600]  @ {DiLeiBox}
// /*-25-*/					[601]  @ {Button}
// /*-25-*/				[602]  @ {DiLeiBox}
// /*-25-*/					[603]  @ {Button}
// /*-25-*/					[604] +@ {LabelTTF} "1"
// /*-25-*/						[605]  @ {Label} "1"
// /*-25-*/							[606]  @ {Sprite}
// /*-25-*/				[607]  @ {DiLeiBox}
// /*-25-*/					[608]  @ {Button}
// /*-25-*/					[609] +@ {LabelTTF} "1"
// /*-25-*/						[610]  @ {Label} "1"
// /*-25-*/							[611]  @ {Sprite}
// /*-25-*/				[612]  @ {DiLeiBox}
// /*-25-*/					[613]  @ {Button}
// /*-25-*/					[614] +@ {LabelTTF} "1"
// /*-25-*/						[615]  @ {Label} "1"
// /*-25-*/							[616]  @ {Sprite}
// /*-25-*/				[617]  @ {DiLeiBox}
// /*-25-*/					[618]  @ {Button}
// /*-25-*/				[619]  @ {DiLeiBox}
// /*-25-*/					[620]  @ {Button}
// /*-25-*/					[621] +@ {LabelTTF} "1"
// /*-25-*/						[622]  @ {Label} "1"
// /*-25-*/							[623]  @ {Sprite}
// /*-25-*/				[624]  @ {DiLeiBox}
// /*-25-*/					[625]  @ {Button}
// /*-25-*/					[626] +@ {LabelTTF} "1"
// /*-25-*/						[627]  @ {Label} "1"
// /*-25-*/							[628]  @ {Sprite}
// /*-25-*/				[629]  @ {DiLeiBox}
// /*-25-*/					[630]  @ {Button}
// /*-25-*/					[631] +@ {LabelTTF} "1"
// /*-25-*/						[632]  @ {Label} "1"
// /*-25-*/							[633]  @ {Sprite}
// /*-25-*/				[634]  @ {DiLeiBox}
// /*-25-*/					[635]  @ {Button}
// /*-25-*/					[636] +@ {Sprite} "src/DiLei/res/common_frame_05.png"
// /*-25-*/				[637]  @ {DiLeiBox}
// /*-25-*/					[638]  @ {Button}
// /*-25-*/					[639] +@ {LabelTTF} "1"
// /*-25-*/						[640]  @ {Label} "1"
// /*-25-*/							[641]  @ {Sprite}
// /*-25-*/				[642]  @ {DiLeiBox}
// /*-25-*/					[643]  @ {Button}
// /*-25-*/				[644]  @ {DiLeiBox}
// /*-25-*/					[645]  @ {Button}
// /*-25-*/				[646]  @ {DiLeiBox}
// /*-25-*/					[647]  @ {Button}
// /*-25-*/				[648]  @ {DiLeiBox}
// /*-25-*/					[649]  @ {Button}
// /*-25-*/				[650]  @ {DiLeiBox}
// /*-25-*/					[651]  @ {Button}
// /*-25-*/				[652]  @ {DiLeiBox}
// /*-25-*/					[653]  @ {Button}
// /*-25-*/				[654]  @ {DiLeiBox}
// /*-25-*/					[655]  @ {Button}
// /*-25-*/				[656]  @ {DiLeiBox}
// /*-25-*/					[657]  @ {Button}
// /*-25-*/			[658]  @ {DiLeiScoreLayer}
// /*-25-*/				[659] +@ {LabelTTF} "Level:1"
// /*-25-*/					[660]  @ {Label} "Level:1"
// /*-25-*/						[661]  @ {Sprite}
// /*-25-*/				[662] +@ {LabelTTF} "Score:91"
// /*-25-*/					[663]  @ {Label} "Score:91"
// /*-25-*/						[664]  @ {Sprite}
// /*-25-*/				[665]  @ {Node}
// /*-25-*/					[666]  @ {Button}
// /*-25-*/					[667] +@ {LabelTTF} "取消标志"
// /*-25-*/						[668]  @ {Label} "取消标志"
// /*-25-*/							[669]  @ {Sprite}
