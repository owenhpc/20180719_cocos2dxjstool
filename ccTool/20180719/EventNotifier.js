/**
 * Created by huangpinqian on 2017/10/19.
 * 單一事件管理者，每個管理者的事件皆為獨立，不會互相呼叫到
 */

var EventGuid = 0;    //事件物件流水號


var EventNotifier = cc.Class.extend({

    listenerCount: 0,   //監聽者數量
    onAddFirstListener: null,   //當加入第一個監聽者時
    onRemoveAllListener: null,  //當移除所有監聽者時
    showDebugLog: false,

    init: function (eventName, showDebugLog, onAddFirstListener, onRemoveAllListener) {
        //流水號
        Object.defineProperty(this, 'eventGuid', { value: EventGuid++, writable: false, });
        //事件名稱
        Object.defineProperty(this, 'eventName', { value: eventName, writable: false, });
        //事件註冊 Key（跟 cocos 註冊的實際事件 key）
        Object.defineProperty(this, 'eventKey', { value: this.eventGuid + "_" + this.eventName, writable: false, });
        //事件註冊物件存放在監聽者上的變數名稱
        Object.defineProperty(this, 'eventListenerObjFieldName', { value: "__EventListenerObj_" + this.eventKey, writable: false, });

        this.showDebugLog = showDebugLog;
        this.onAddFirstListener = onAddFirstListener;
        this.onRemoveAllListener = onRemoveAllListener;
    },

    //加入監聽
    //監聽方法中的 this == listener
    addListener: function (listener, func) {
        if(!listener || !func){
            cc.log("[error][EventNotifier] 加入監聽的物件錯誤：", this.eventKey, listener);
            return;
        }

        if(listener[this.eventListenerObjFieldName]){
            cc.log("[error][EventNotifier] 重複加入監聽：", this.eventKey, listener);
            return;
        }

        if(this.showDebugLog)
            cctool.logStack("[event] addListener Event: " + this.eventListenerObjFieldName);

        //實際監聽方法（轉接一次，以設定當前監聽物件）
        var listenFunc = function (evt) {
            func.call(listener, evt);
        };
        var listenObj = cc.eventManager.addCustomListener(this.eventKey, listenFunc); //cocos 的事件註冊物件
        listener[this.eventListenerObjFieldName] = listenObj;   //直接存在監聽者上

        this.listenerCount++;
        if(this.listenerCount === 1 && this.onAddFirstListener)
            this.onAddFirstListener();

    },

    //單獨移除指定監聽者的監聽
    removeListener: function (listener) {
        var listenObj = listener[this.eventListenerObjFieldName];
        if(!listenObj)
            return;

        if(this.showDebugLog)
            cctool.logStack("[event] removeListener Event: " + this.eventListenerObjFieldName);

        delete listener[this.eventListenerObjFieldName];
        cc.eventManager.removeListener(listenObj);

        this.listenerCount--;
        if(this.listenerCount <= 0){
            this.listenerCount = 0;
            if(this.onRemoveAllListener)
                this.onRemoveAllListener();
        }
    },

    //移除所有監聽
    clearListener: function () {
        // cctool.logStack("清除監聽：" + this.eventKey);
;        cc.eventManager.removeCustomListeners(this.eventKey);
        if(this.listenerCount > 0) {
            this.listenerCount = 0;
            if (this.onRemoveAllListener)
                this.onRemoveAllListener();
        }
    },

    //發出事件通知
    dispatchEvent: function (param) {
        if(this.listenerCount <= 0)
            return;

        if(this.showDebugLog)
            cctool.logStack("[event] dispatch Event: " + this.eventListenerObjFieldName);

        cc.eventManager.dispatchCustomEvent(this.eventKey, param);
    },

});

EventNotifier.create = function (eventKey, showDebugLog = true, onAddFirstListener = null, onRemoveAllListener = null) {
    var inst = new EventNotifier();
    inst.init(eventKey, showDebugLog, onAddFirstListener, onRemoveAllListener);
    return inst;
};



/*純測試用方法
EventNotifier.TestFunc = function () {
    var objA = {
        func: function (evt) {
            cc.log("[owen] test event,", this);
            cc.log("[owen] test event, objA", evt.getUserData());
        }
    };
    var objB = {
        func: function (evt) {
            cc.log("[owen] test event, objB", evt.getUserData());
        }
    };

    function onAddFirstListener() {
        cc.log("[owen] test event, onAddFirstListener !");
    };
    function onRemoveAllListener() {
        cc.log("[owen] test event, onRemoveAllListener !");
    };

    var eventObj = EventNotifier.create("eventObjA", onAddFirstListener, onRemoveAllListener);
    eventObj.addListener(objA, objA.func);
    eventObj.addListener(objB, objB.func);
    eventObj.dispatchEvent("dispatchEvent!! 1111");

    eventObj.removeListener(objA);
    eventObj.removeListener(objA);
    eventObj.dispatchEvent("dispatchEvent!! 2222");

    eventObj.clearListener();
    eventObj.dispatchEvent("dispatchEvent!! 3333");

    // 結果：
    //     [owen] test event, onAddFirstListener !
    //     [owen] test event, this [object EventDispatcher]
    //     [owen] test event, objA dispatchEvent!! 1111
    //     [owen] test event, objB dispatchEvent!! 1111
    //     [owen] test event, objB dispatchEvent!! 2222
    //     [owen] test event, onRemoveAllListener !
};
//*/