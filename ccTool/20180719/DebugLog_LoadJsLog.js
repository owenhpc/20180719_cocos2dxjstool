/**
 * Created by huangpinqian on 2018/7/31.
 * 攔截並顯示載入 js 檔時的訊息
 */




var cctool = cctool || {};
(function(module){
    module.DebugLog_LoadJsLog = true;

    /** 當載入 js 時事件 **/
    var onLoadJsEvent = EventNotifier.create("onLoadJsEvent");
    cctool.onLoadJsEvent = onLoadJsEvent;

    var enableLog = false;   //開關，會很多 Log !

    var org_Require = require;
    require = function () {
        var path = arguments[0];

        if(enableLog)
            cctool.logStack("[owen] 載入 js (require): " + path);

        var returnObj = cctool.callFunctionWithArguments(org_Require, this, arguments);

        onLoadJsEvent.dispatchEvent(path);

        return returnObj;
    };

    var org_loadJs = cc.loader.loadJs;
    cc.loader.loadJs = function () {
        var path = arguments[0];
        var files = arguments[1];

        if(enableLog)
            cctool.logStack("[owen] 載入 js (loadJs): " + path + ", files: " + files);

        var returnObj = cctool.callFunctionWithArguments(org_loadJs, this, arguments);

        if(path && files){
            for(var i=0; i<files.length; i++){
                var file = files[i];
                if(file)
                    onLoadJsEvent.dispatchEvent(path + "/" + file);
            }
        }

        return returnObj;
    };


})(cctool);

