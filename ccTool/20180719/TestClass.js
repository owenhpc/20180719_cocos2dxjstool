/**
 * Created by huangpinqian on 2018/7/19.
 * 用來測試的 Class
 */


var TestClass = cc.Node.extend({

    ctor: function (str) {

        cc.log("[ooo] TestClass ctor, " + str);
        this._super();
    },

    TestClass_func: function (str) {
        cc.log("TestClass_func !!!!!!", str);
    },

});


var TestClass2 = TestClass.extend({

    ctor: function (str) {
        cc.log("[ooo] TestClass2 ctor, " + str);
        this._super();
    },

    TestClass_func2: function (str) {
        cc.log("TestClass_func2 !!!!!!", str);
    },

});


cc.log("載入 TestClass " + TestClass);

