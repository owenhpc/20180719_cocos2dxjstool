/**
 * Created by huangpinqian on 2018/06/28.
 * 快速建立按鈕的工具
 */

var cctool = cctool || {};
(function(module){
    module.BtnTool = true;

    /** 加入圖背景 + 文字按鈕 **/
    module.AddBtn_TextureText = function (parent, fontSize, fontColor, mes, picPath, scale, pos, clickFunc) {
        var btnNode = new cc.Node();
        btnNode.setPosition(pos);
        parent.addChild(btnNode);

        var btn = cctool.AddBtn_Texture(btnNode, picPath, scale, cc.p(0, 0), clickFunc);
        btn.setPressedActionEnabled(false);       //關閉點擊縮放效果（好像沒用）
        btn.setZoomScale(0);       //關閉點擊縮放效果

        var label = new cc.LabelTTF(mes, "Arial", fontSize);
        label.color = fontColor;
        label.scale = scale;
        label.setAnchorPoint(0.5, 0.5);
        btnNode.addChild(label);
        return { node: btnNode, btn: btn, label: label };
    };

    /** 加入單張圖按鈕 **/
    module.AddBtn_Texture = function (parent, picPath, scale, pos, clickFunc) {
        var btn = new ccui.Button();
        btn.loadTextureNormal(picPath, ccui.Widget.LOCAL_TEXTURE);  //先設定避免 color 設定時跳錯誤
        btn.setTouchEnabled(true);    //預設開啟點擊監聽
        // btn.setPressedActionEnabled(false);       //關閉點擊縮放效果（好像沒用）
        // btn.setZoomScale(0);       //關閉點擊縮放效果
        btn.scale = scale;
        btn.setAnchorPoint(0.5, 0.5);
        btn.setPosition(pos);
        parent.addChild(btn);

        btn.addTouchEventListener(function (node, type) {
            if(type === ccui.Widget.TOUCH_ENDED)
                clickFunc(btn);
        }, btn);
        return btn;
    };

    /** 加入組合圖按鈕 **/
    module.AddBtn_SpriteA = function (parent, picName, clickFuncCaller, clickFunc) {
        return cctool.AddBtn_SpriteB(parent, picName, 1, 1, 0.5, 0.5, cc.p(0, 0), clickFuncCaller, clickFunc);
    };

    /** 加入組合圖按鈕 **/
    module.AddBtn_SpriteB = function (parent, picName, scaleX, scaleY, anchorX, anchorY, pos, clickFuncCaller, clickFunc) {
        var btn = new ccui.Button();
        btn.loadTextureNormal(picName, ccui.Widget.PLIST_TEXTURE);  //先設定避免 color 設定時跳錯誤
        btn.setTouchEnabled(true);    //預設開啟點擊監聽
        // btn.setPressedActionEnabled(false);       //關閉點擊縮放效果（好像沒用）
        // btn.setZoomScale(0);       //關閉點擊縮放效果
        btn.scaleX = scaleX;
        btn.scaleY = scaleY;
        btn.setAnchorPoint(cc.p(anchorX, anchorY));
        btn.setPosition(pos);
        parent.addChild(btn);

        btn.addTouchEventListener(function (node, type) {
            if(type === ccui.Widget.TOUCH_ENDED)
                clickFunc.call(clickFuncCaller, btn);
        }, btn);
        return btn;
    };

    /** 加入文字按鈕 **/
    module.AddBtn_Text = function (parent, mes, pos, color, clickFunc) {

        var btn = cc.ControlButton.create();
        btn.getTitleLabel().setString(mes);
        btn.getTitleLabel().setSystemFontSize(40);
        // 沒用
        // btn.getTitleLabel().setColor(color);
        // btn.getTitleLabel().color = color;
        // btn.setTitleColorForState(color, 0xFF);
        // /**
        //     第二個參數 state 定義
        //     enum class State
        //     {
        //         NORMAL         = 1 << 0, // The normal, or default state of a control°™that is, enabled but neither selected nor highlighted.
        //         HIGH_LIGHTED   = 1 << 1, // Highlighted state of a control. A control enters this state when a touch down, drag inside or drag enter is performed. You can retrieve and set this value through the highlighted property.
        //         DISABLED       = 1 << 2, // Disabled state of a control. This state indicates that the control is currently disabled. You can retrieve and set this value through the enabled property.
        //         SELECTED       = 1 << 3  // Selected state of a control. This state indicates that the control is currently selected. You can retrieve and set this value through the selected property.
        //     };
        //  */
        // cc.log("[xxx] color aaa", cctool.getObjLog(color));
        // cc.log("[xxx] color bbb", cctool.getObjLog(btn.getTitleLabel().getColor()));
        // btn.getBackgroundSprite().initWithSpriteFrameName("fish_netdisconnect_tips_bg.png", cc.rect(25, 25, 412 - 50, 77 - 50));
        btn.setPreferredSize(cc.size(btn.getTitleLabel().getContentSize().width + 50, 77));
        btn.setPosition(pos);
        parent.addChild(btn, 999999);

        btn.addTargetWithActionForControlEvents(parent, clickFunc, cc.CONTROL_EVENT_TOUCH_UP_INSIDE);
    };

})(cctool);


