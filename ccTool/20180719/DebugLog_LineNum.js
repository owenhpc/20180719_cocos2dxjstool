/**
 * Created by huangpinqian on 2018/5/18.
 * 負責統一處理 log 行號顯示，過長的 log 處理
 */




var cctool = cctool || {};

(function(module){
    module.DebugLog_LineNum = true;

    if(module.orglog === undefined ){
        module.orglog = cc.log;
    }
    var orglog = module.orglog;

    /** Log 工具建立 **/
    var logNo = 0;

    var logMaxLength = 15000;   //xcode 模擬器 16300 左右
    if(cc.sys.os === cc.sys.OS_ANDROID)
        logMaxLength = 500;     //android 訊息好像更短

    //處理過長的 log 狀況，每次最多印 logMaxLength 字元的下一個斷行字數
    var cclog = function (log) {
        if(cc.sys.os === cc.sys.OS_ANDROID) {
            log = "\n" + log;   //android studio 看 log 時，第一行會有很長的標頭...
        }

        if(log.length < logMaxLength){
            orglog(log);
            return;
        }
        var firstLog = true;
        while(true){
            if(!firstLog){
                if(cc.sys.os === cc.sys.OS_ANDROID){
                    log = "<~>\n" + log;   //android studio 看 log 時，第一行會有很長的標頭...
                }else{
                    log = "<~>" + log;
                }
            }


            if(log.length < logMaxLength){
                orglog(log);
                return;

            }else{
                var ignoreNextChat = true;
                var cutIdx = log.indexOf("\n", logMaxLength);
                if(cutIdx === -1){
                    //如果沒有換行符號就找逗號
                    ignoreNextChat = false;
                    cutIdx = log.indexOf(",", logMaxLength);
                    if(cutIdx !== -1)
                        cutIdx ++;  //在逗號的下一個資源換行
                }
                if(cutIdx === -1){
                    //如果都沒有就直接強制斷行
                    ignoreNextChat = false;
                    cutIdx = logMaxLength;
                }

                orglog(log.substring(0, cutIdx) + "<~>");
                if(ignoreNextChat){
                    log = log.substring(cutIdx+1, log.length);  //cutIdx+1 跳過此換行符號
                }else{
                    log = log.substring(cutIdx, log.length);
                }
                // log += "<~>";
            }
            firstLog = false;
        }
    };

    //重設原本 log
    console.log = log = cc.log = function () {
        var logs = null;
        var tag = null;
        var addStack = false;
        for (var i = 0; i < arguments.length; i++) {
            var argument = null;
            if (arguments[i] === null) {
                argument = "<null>";
            } else if (arguments[i] === undefined) {
                argument = "<undefined>";
            } else {
                argument = arguments[i].toString();
            }

            if (!tag) {
                if(argument.indexOf("FFFFFFFFFFFUUUUUUUUUUUUUCCCCCCCCCCCKKKKKKK") !== -1) {
                    //666 專案特別排除處理（不知道誰印的，煩死了）
                }else if(argument.indexOf("is not a function") !== -1) {
                    cclog("攔截到 this._super is not a function");
                    tag = "[!! error !!]";
                    addStack = true;

                } else if (argument.indexOf("error") !== -1
                    || argument.indexOf("Error") !== -1
                    || argument.indexOf("ERROR") !== -1) {
                    tag = "[!! error !!]";
                    addStack = true;

                } else if (argument.indexOf("warning") !== -1
                    || argument.indexOf("Warning") !== -1
                    || argument.indexOf("WARNING") !== -1) {
                    tag = "[warning]";

                }
            }

            if (logs) {
                logs += " " + argument;
            } else {
                logs = argument;
            }
        }

        if (addStack) {
            //標示要加入堆疊資料
            logs += "\nStack:" + cctool.getStackLog(true);
        }

        //加入標籤
        if (tag) {
            logs = tag + " " + logs;
        }

        //加入行號
        logs = "/*-" + (logNo) + "---*/\t" + logs;
        //換行加小行號
        logs = logs.replace(/\n/g, "\n/*-" + logNo + "-*/\t\t");

        logNo++;

        //輸出
        cclog(logs);
    };

})(cctool);



// Log 流水號顯示範例
// 可過濾 "-6-" 來只顯示第 6 次輸出 Log，非 Js 印出的目前無法攔截，所以 Error 還是要單獨過濾來看
// Error 會特別標出 Tag

// JS: [owen] 已啟動工具：TestClass.js
// JS: [owen] 已啟動工具：EventNotifier.js
// JS: [owen] 已啟動工具：DebugLog.js
// JS: /*-0---*/	[owen] 已啟動工具：DebugLog_LineNum.js
// JS: /*-1---*/	[owen] 已啟動工具：ShowTargetCenter.js
// JS: /*-2---*/	[owen] 已啟動工具：CreateStackLogTool.js
// JS: /*-3---*/	[owen] 已啟動工具：EventListenerTool.js
// JS: /*-4---*/	[owen] 已啟動工具：BtnTool.js
// JS: /*-5---*/	[owen] 已啟動工具：DebugLog_LoadJsLog.js
// JS: /*-6---*/	[!! error !!] [Error] 啟動工具失敗：src/ccTool/20180719/DebugLog_666.js
// /*-6-*/		Message：can't open : No such file or directory
// /*-6-*/		Stack：module.init@/Users/huangpinqian/Library/Developer/CoreSimulator/Devices/D3A1669B-F8F2-4D48-8B62-87DF41929BB0/data/Containers/Bundle/Application/2FA079B1-CA19-4A4C-BAE2-DC1D3E6CD4AE/DiLei iOS.app/src/ccTool/20180719/ToolsManager.js:103
// /*-6-*/		checkEnableTool@/Users/huangpinqian/Library/Developer/CoreSimulator/Devices/D3A1669B-F8F2-4D48-8B62-87DF41929BB0/data/Containers/Bundle/Application/2FA079B1-CA19-4A4C-BAE2-DC1D3E6CD4AE/DiLei iOS.app/main.js:69
// /*-6-*/		cc.game.onStart@/Users/huangpinqian/Library/Developer/CoreSimulator/Devices/D3A1669B-F8F2-4D48-8B62-87DF41929BB0/data/Containers/Bundle/Application/2FA079B1-CA19-4A4C-BAE2-DC1D3E6CD4AE/DiLei iOS.app/main.js:51
// /*-6-*/		cc.game.run/<@/Users/huangpinqian/Library/Developer/CoreSimulator/Devices/D3A1669B-F8F2-4D48-8B62-87DF41929BB0/data/Containers/Bundle/Application/2FA079B1-CA19-4A4C-BAE2-DC1D3E6CD4AE/DiLei iOS.app/script/jsb_boot.js:1531
// /*-6-*/		cc.game.prepare/<@/Users/huangpinqian/Library/Developer/CoreSimulator/Devices/D3A1669B-F8F2-4D48-8B62-87DF41929BB0/data/Containers/Bundle/Application/2FA079B1-CA19-4A4C-BAE2-DC1D3E6CD4AE/DiLei iOS.app/script/jsb_boot.js:1602
// /*-6-*/		cc.loader.loadJs@/Users/huangpinqian/Library/Developer/CoreSimulator/Devices/D3A1669B-F8F2-4D48-8B62-87DF41929BB0/data/Containers/Bundle/Application/2FA079B1-CA19-4A4C-BAE2-DC1D3E6CD4AE/DiLei iOS.app/script/jsb_boot.js:517
// /*-6-*/		cc.loader.loadJsWithImg@/Users/huangpinqian/Library/Developer/CoreSimulator/Devices/D3A1669B-F8F2-4D48-8B62-87DF41929BB0/data/Containers/Bundle/Application/2FA079B1-CA19-4A4C-BAE2-DC1D3E6CD4AE/DiLei iOS.app/script/jsb_boot.js:526
// /*-6-*/		cc.game.prepare@/Users/huangpinqian/Library/Developer/CoreSimulator/Devices/D3A1669B-F8F2-4D48-8B62-87DF41929BB0/data/Containers/Bundle/Application/2FA079B1-CA19-4A4C-BAE2-DC1D3E6CD4AE/DiLei iOS.app/script/jsb_boot.js:1599
// /*-6-*/		cc.game.run@/Users/huangpinqian/Library/Developer/CoreSimulator/Devices/D3A1669B-F8F2-4D48-8B62-87DF41929BB0/data/Containers/Bundle/Application/2FA079B1-CA19-4A4C-BAE2-DC1D3E6CD4AE/DiLei iOS.app/script/jsb_boot.js:1530
// /*-6-*/		@/Users/huangpinqian/Library/Developer/CoreSimulator/Devices/D3A1669B-F8F2-4D48-8B62-87DF41929BB0/data/Containers/Bundle/Application/2FA079B1-CA19-4A4C-BAE2-DC1D3E6CD4AE/DiLei iOS.app/main.js:73
// /*-6-*/
// /*-6-*/
// /*-6-*/		Stack:cc.log@/Users/.../DiLei iOS.app/src/ccTool/20180719/DebugLog_LineNum.js:125
// /*-6-*/		module.init@/Users/.../DiLei iOS.app/src/ccTool/20180719/ToolsManager.js:106
// /*-6-*/		checkEnableTool@/Users/.../DiLei iOS.app/main.js:69
// /*-6-*/		cc.game.onStart@/Users/.../DiLei iOS.app/main.js:51
// /*-6-*/		cc.game.run/<@/Users/.../DiLei iOS.app/script/jsb_boot.js:1531
// /*-6-*/		cc.game.prepare/<@/Users/.../DiLei iOS.app/script/jsb_boot.js:1602
// /*-6-*/		cc.loader.loadJs@/Users/.../DiLei iOS.app/script/jsb_boot.js:517
// /*-6-*/		cc.loader.loadJsWithImg@/Users/.../DiLei iOS.app/script/jsb_boot.js:526
// /*-6-*/		cc.game.prepare@/Users/.../DiLei iOS.app/script/jsb_boot.js:1599
// /*-6-*/		cc.game.run@/Users/.../DiLei iOS.app/script/jsb_boot.js:1530
// /*-6-*/		@/Users/.../DiLei iOS.app/main.js:73
// /*-6-*/
// JS: /*-7---*/	[owen] 已啟動工具：DebugLog_Hierarchy.js
// JS: /*-8---*/	[owen] 已啟動工具：ViewObjCatchData.js
// JS: /*-9---*/	[owen] 已啟動工具：DebugLog_CallMethodInfo.js
// JS: /*-10---*/	[owen] 已啟動工具：ShowChangeSceneLog.js
// JS: /*-11---*/	轉場景 runScene: DiLeiGameScene [object Object]
// /*-11-*/		stack: cc.director.runScene@/Users/.../DiLei iOS.app/src/ccTool/20180719/ShowChangeSceneLog.js:15
// /*-11-*/		cc.game.onStart/<@/Users/.../DiLei iOS.app/main.js:57
// /*-11-*/		cc.Loader<.initWith@/Users/.../DiLei iOS.app/script/jsb_cocos2d.js:1475
// /*-11-*/		cc.Loader.preload@/Users/.../DiLei iOS.app/script/jsb_cocos2d.js:1483
// /*-11-*/		cc.game.onStart@/Users/.../DiLei iOS.app/main.js:58
// /*-11-*/		cc.game.run/<@/Users/.../DiLei iOS.app/script/jsb_boot.js:1531
// /*-11-*/		cc.game.prepare/<@/Users/.../DiLei iOS.app/script/jsb_boot.js:1602
// /*-11-*/		cc.loader.loadJs@/Users/.../DiLei iOS.app/script/jsb_boot.js:517
// /*-11-*/		cc.loader.loadJsWithImg@/Users/.../DiLei iOS.app/script/jsb_boot.js:526
// /*-11-*/		cc.game.prepare@/Users/.../DiLei iOS.app/script/jsb_boot.js:1599
// /*-11-*/		cc.game.run@/Users/.../DiLei iOS.app/script/jsb_boot.js:1530
// /*-11-*/		@/Users/.../DiLei iOS.app/main.js:73
// /*-11-*/
// 2018-09-25 10:35:02.971 DiLei iOS[23112:1562005] cocos2d: surface size: 640x1136
// 2018-09-25 10:35:03.033 DiLei iOS[23112:1562005] cocos2d: surface size: 640x1136
// JS: /*-12---*/	[owen] 點擊事件！！ Stack:
// /*-12-*/		_showTargetCenter/listener<.onTouchBegan@/Users/.../DiLei iOS.app/src/ccTool/20180719/ShowTargetCenter.js:243
// /*-12-*/		cc.eventManager.addListener/listenData.onTouchBegan@/Users/.../DiLei iOS.app/src/ccTool/20180719/EventListenerTool.js:54
// /*-12-*/
